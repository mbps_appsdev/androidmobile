package com.mbps.idp_journal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class LeaderboardsAdapter extends RecyclerView.Adapter<LeaderboardsAdapter.ViewHolder>  {

    View.OnClickListener mClickListener;
    Context mContext;
    LayoutInflater inflater;

    List<LeaderboardItem> top3;
    List<LeaderboardItem> items;

    public LeaderboardsAdapter(Context mContext, List<LeaderboardItem> _items) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(mContext);
        items = _items;
        top3 = new ArrayList<>();

        //get top 3
        for(int i = 0; i < 3; i++) {
            if(items.size() > 0 && top3.size() < 3) {
                top3.add(items.get(0));
                items.remove(0);
            }
        }

        Log.e("ERROR", "[" + top3.size() + "]constructor: " + items.size());

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        public ViewHolder(View view){
            super(view);
            layout = (LinearLayout) view;
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(position == 0) {
            return 1; //top
        }
        else {
            return 0;
        }
    }

    @Override
    public LeaderboardsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {


        if(viewType == 1) {
            LinearLayout itemView = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.topheader_leaderboard_layout, viewGroup, false);

            LeaderboardsAdapter.ViewHolder holder = new LeaderboardsAdapter.ViewHolder(itemView);

            return (LeaderboardsAdapter.ViewHolder) holder;
        }
        else {
            LinearLayout itemView = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.card_leaderboard_layout, viewGroup, false);

            LeaderboardsAdapter.ViewHolder holder = new LeaderboardsAdapter.ViewHolder(itemView);

            return (LeaderboardsAdapter.ViewHolder) holder;
        }

    }

    @Override
    public void onBindViewHolder(LeaderboardsAdapter.ViewHolder viewHolder, int i) {


        if(i == 0) {
            //topheader

            RelativeLayout layout1 = (RelativeLayout) viewHolder.layout.findViewById(R.id.layoutTop1);
            TextView tvRank1 = (TextView) viewHolder.layout.findViewById(R.id.textTop1);
            ImageView imgAvatarPic1 = (ImageView) viewHolder.layout.findViewById(R.id.imgProfilePic1);
            TextView tvUsername1 = (TextView) viewHolder.layout.findViewById(R.id.textName1);
            TextView tvBadgePoints1 = (TextView) viewHolder.layout.findViewById(R.id.textPoints1);

            RelativeLayout layout2 = (RelativeLayout) viewHolder.layout.findViewById(R.id.layoutTop2);
            TextView tvRank2 = (TextView) viewHolder.layout.findViewById(R.id.textTop2);
            ImageView imgAvatarPic2 = (ImageView) viewHolder.layout.findViewById(R.id.imgProfilePic2);
            TextView tvUsername2 = (TextView) viewHolder.layout.findViewById(R.id.textName2);
            TextView tvBadgePoints2 = (TextView) viewHolder.layout.findViewById(R.id.textPoints2);

            RelativeLayout layout3 = (RelativeLayout) viewHolder.layout.findViewById(R.id.layoutTop3);
            TextView tvRank3 = (TextView) viewHolder.layout.findViewById(R.id.textTop3);
            ImageView imgAvatarPic3 = (ImageView) viewHolder.layout.findViewById(R.id.imgProfilePic3);
            TextView tvUsername3 = (TextView) viewHolder.layout.findViewById(R.id.textName3);
            TextView tvBadgePoints3 = (TextView) viewHolder.layout.findViewById(R.id.textPoints3);


            imgAvatarPic1.setImageResource(0);
            imgAvatarPic2.setImageResource(0);
            imgAvatarPic3.setImageResource(0);

            layout1.setVisibility(View.INVISIBLE);
            layout2.setVisibility(View.INVISIBLE);
            layout3.setVisibility(View.INVISIBLE);

            for(int idx = 0; idx < 3; idx++) {

                if(top3.size() > idx) {

                    LeaderboardItem item = (LeaderboardItem) top3.get(idx);

                    if(idx == 0) {
                        tvRank1.setText(item.UserRank);
                        tvUsername1.setText(item.UserName);
                        tvBadgePoints1.setText(String.valueOf(Math.round(Float.valueOf(item.UserBadgePoints))));
                        layout1.setVisibility(View.VISIBLE);
                        setAvatar(imgAvatarPic1, item.UserAvatarPath);
                    }
                    else if(idx == 1) {
                        tvRank2.setText(item.UserRank);
                        tvUsername2.setText(item.UserName);
                        tvBadgePoints2.setText(String.valueOf(Math.round(Float.valueOf(item.UserBadgePoints))));
                        layout2.setVisibility(View.VISIBLE);
                        setAvatar(imgAvatarPic2, item.UserAvatarPath);
                    }
                    else if(idx == 2) {
                        tvRank3.setText(item.UserRank);
                        tvUsername3.setText(item.UserName);
                        tvBadgePoints3.setText(String.valueOf(Math.round(Float.valueOf(item.UserBadgePoints))));
                        layout3.setVisibility(View.VISIBLE);
                        setAvatar(imgAvatarPic3, item.UserAvatarPath);
                    }
                }
            }

        }
        else {

            TextView tvRank = (TextView) viewHolder.layout.findViewById(R.id.textRankNum);
            ImageView imgAvatarPic = (ImageView) viewHolder.layout.findViewById(R.id.imgProfilePic);
            TextView tvUsername = (TextView) viewHolder.layout.findViewById(R.id.textUsername);
            TextView tvBadgePoints = (TextView) viewHolder.layout.findViewById(R.id.textBadgePoints);

            LeaderboardItem item = (LeaderboardItem) items.get(i - 1);

            tvRank.setText(item.UserRank);
            tvUsername.setText(item.UserName);
            tvBadgePoints.setText(String.valueOf(Math.round(Float.valueOf(item.UserBadgePoints))));

            if(item.UserAvatarPath == null){
                imgAvatarPic.setImageResource(0);
                return;
            }

            System.out.println("USERAVATARPATH::::: " + item.UserAvatarPath);

            if(isNullOrEmpty(item.UserAvatarPath)){
                imgAvatarPic.setImageResource(0);
                return;
            }

            switch (item.UserAvatarPath){
                case "agirl1":
                    imgAvatarPic.setImageResource(R.drawable.agirl1);
                    break;

                case "agirl2":
                    imgAvatarPic.setImageResource(R.drawable.agirl2);
                    break;

                case "agirl3":
                    imgAvatarPic.setImageResource(R.drawable.agirl3);
                    break;

                case "agirl4":
                    imgAvatarPic.setImageResource(R.drawable.agirl4);
                    break;

                case "agirl5":
                    imgAvatarPic.setImageResource(R.drawable.agirl5);
                    break;

                case "agirl6":
                    imgAvatarPic.setImageResource(R.drawable.agirl6);
                    break;

                case "aguy1":
                    imgAvatarPic.setImageResource(R.drawable.aguy1);
                    break;

                case "aguy2":
                    imgAvatarPic.setImageResource(R.drawable.aguy2);
                    break;

                case "aguy3":
                    imgAvatarPic.setImageResource(R.drawable.aguy3);
                    break;

                case "aguy4":
                    imgAvatarPic.setImageResource(R.drawable.aguy4);
                    break;

                case "aguy5":
                    imgAvatarPic.setImageResource(R.drawable.aguy5);
                    break;

                case "aguy6":
                    imgAvatarPic.setImageResource(R.drawable.aguy6);
                    break;

                default:
                    break;
            }

        }

    }

    @Override
    public int getItemCount() {
        return 1 + items.size();
    }


    private void setAvatar(ImageView imageView, String avatar){

        if(isNullOrEmpty(avatar))
        {
            imageView.setImageResource(0);
            return;
        }


        switch (avatar){
            case "agirl1":
                imageView.setImageResource(R.drawable.agirl1);
                break;

            case "agirl2":
                imageView.setImageResource(R.drawable.agirl2);
                break;

            case "agirl3":
                imageView.setImageResource(R.drawable.agirl3);
                break;

            case "agirl4":
                imageView.setImageResource(R.drawable.agirl4);
                break;

            case "agirl5":
                imageView.setImageResource(R.drawable.agirl5);
                break;

            case "agirl6":
                imageView.setImageResource(R.drawable.agirl6);
                break;

            case "aguy1":
                imageView.setImageResource(R.drawable.aguy1);
                break;

            case "aguy2":
                imageView.setImageResource(R.drawable.aguy2);
                break;

            case "aguy3":
                imageView.setImageResource(R.drawable.aguy3);
                break;

            case "aguy4":
                imageView.setImageResource(R.drawable.aguy4);
                break;

            case "aguy5":
                imageView.setImageResource(R.drawable.aguy5);
                break;

            case "aguy6":
                imageView.setImageResource(R.drawable.aguy6);
                break;

            default:
                break;
        }
    }

    public static boolean isNullOrEmpty(String str) {
        if(str != null && !str.trim().isEmpty())
            return false;
        return true;
    }
}
