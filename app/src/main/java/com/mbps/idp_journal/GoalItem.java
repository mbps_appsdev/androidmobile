package com.mbps.idp_journal;

public class GoalItem {
    String competencyId;
    String competencyTitle;
    String goalDueDate;
    String goalDescription;
    String goalActionPlan;
    String goalResources;
    String goalSuccessMeasure1;
    String goalSuccessMeasure2;
    String goalSuccessMeasure3;
    String goalCompletionDate;
    String goalActualCompletionDate;
    String goalCreatedBy;
    String goalCreatedDated;
    String goalModifiedBy;
    String goalModifiedDate;
    Double goalProgress;


    public GoalItem() {
        this.competencyTitle = "";
        this.goalDueDate = "";
        this.goalDescription = "";
        this.goalActionPlan = "";
        this.goalResources = "";
        this.goalSuccessMeasure1 = "";
        this.goalSuccessMeasure2 = "";
        this.goalSuccessMeasure3 = "";
        this.goalCompletionDate = "";
        this.goalActualCompletionDate = "";
        this.goalCreatedBy = "";
        this.goalCreatedDated = "";
        this.goalModifiedBy = "";
        this.goalModifiedDate = "";
        this.goalProgress = 0.0;
    }

}
