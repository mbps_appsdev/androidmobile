package com.mbps.idp_journal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mbps.idp_journal.api.APIRestClient;
import com.mbps.idp_journal.api.NetworkHelper;
import com.mbps.idp_journal.barcode.BarcodeCaptureActivity;
import com.mbps.idp_journal.model.BadgesItem;
import com.mbps.idp_journal.model.CompetencyBadges;
import com.mbps.idp_journal.model.Users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;

public class BadgesActivity extends BaseActivity {

    private static final int BARCODE_READER_REQUEST_CODE = 1;
    private static final int QUESTION_READER_REQUEST_CODE = 2;
    BadgesAdapter adapter1, adapter2;
    Spinner spinner;
    ProgressBar progressBar;
    ArrayList<BadgesItem> individualizedBadgesList = new ArrayList<>();
    ArrayList<BadgesItem> sparksBadgesList = new ArrayList<>();
    ArrayList<CompetencyBadges> competencyBadgesList = new ArrayList<>();
    ArrayList<CompetencyBadges> sparksBadgesListById = new ArrayList<>();
    ExpandableHeightGridView gridView, gridViewSparks;
    com.mbps.idp_journal.Helper_Dialog errorDialog, successDialog;

    public static String qrCodeScanned = "";
    private String qrCodeToCheck = "";

    boolean isNoMatch = true;

    @Override
    public void onResume(){
        super.onResume();

//        Barcode barcode;
//
//        Bundle extras = getIntent().getExtras();
//        if(extras == null) {
//            barcode = null;
//        } else {
//            barcode = (Barcode) extras.get(Constants.BarcodeObject);
//            System.out.println("RETURNED BARCODE:::::" + barcode.displayValue);
//            Toast.makeText(getApplicationContext(), barcode.displayValue.toString(), Toast.LENGTH_LONG).show();
//        }


        if(this.qrCodeScanned.length() > 0) {

            showLoading();
            if(this.qrCodeToCheck.length() > 0 && this.qrCodeToCheck.equals(this.qrCodeScanned)) {

                RequestParams params = new RequestParams();
                params.put("UserId", kConstants.getLoggedInUser().UserId);
                params.put("QRCode", this.qrCodeScanned);

                System.out.println("QRCode:::::: " + this.qrCodeScanned);


                APIRestClient.client(BadgesActivity.this).post("api/Sparks/claimBadge", params, new AsyncHttpResponseHandler() {


                    @Override
                    public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                        AlertDialog.Builder dialog = new AlertDialog.Builder(BadgesActivity.this);
                        dialog.setCancelable(false);
                        dialog.setTitle("Success!");
                        dialog.setMessage("Successfully Claimed Badge!");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alert = dialog.create();
                        alert.show();

                    }


                    @Override
                    public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                        System.out.println("FAILED " + statusCode);

                        AlertDialog.Builder dialog = new AlertDialog.Builder(BadgesActivity.this);
                        dialog.setCancelable(false);
                        dialog.setTitle("Failed!");
                        dialog.setMessage("Invalid QR Code Badge!");
                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                        final AlertDialog alert = dialog.create();
                        alert.show();

                    }


                    @Override
                    public void onFinish() {

                        super.onFinish();

                        qrCodeScanned = "";
                        qrCodeToCheck = "";

                        hideLoading();
                    }
                });
            }
            else {

                hideLoading();
                AlertDialog.Builder dialog = new AlertDialog.Builder(BadgesActivity.this);
                dialog.setCancelable(false);
                dialog.setTitle("Failed!");
                dialog.setMessage("Invalid QR Code Badge!");
                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                });
                final AlertDialog alert = dialog.create();
                alert.show();

                qrCodeScanned = "";
                qrCodeToCheck = "";
            }
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_badges);
        spinner = findViewById(R.id.spinnerBadges);

//        getAllCompetencyBadges();
        getAllCompetencyBadgesById(kConstants.getLoggedInUser().UserId);
        getSparksBadges();

        gridView = (ExpandableHeightGridView) findViewById(R.id.gridviewIndividualizedBadges);
        gridView.setExpanded(true);

        gridViewSparks = (ExpandableHeightGridView) findViewById(R.id.gridviewSparksBadges);
        gridViewSparks.setExpanded(true);

        progressBar = (ProgressBar) findViewById(R.id.progress);

        ImageView menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
            }
        });

        ImageView homeButton = findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHome();
            }
        });


        gridViewSparks.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (NetworkHelper.isInternetAvailable(getApplicationContext())){

                    qrCodeToCheck = sparksBadgesListById.get(position).QRCode;

                    Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);

                    startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
                }else{
                    showErrorMessage(getResources().getString(R.string.interner_error));
                }
            }
        });

        setSpinnerAdapter();

    }

    private void setSpinnerAdapter(){
        ArrayList<String> spinnerAdapterData = new ArrayList<>();
        final String[] spinnerItemsArray = getResources().getStringArray(R.array.badges_array);
        Collections.addAll(spinnerAdapterData, spinnerItemsArray);
//        SpinnerAdapter adapter = new BadgesSpinnerAdapter(this, R.layout.badges_spinner_layout, spinnerAdapterData, getResources());

        SpinnerAdapter adapter = new com.mbps.idp_journal.SpinnerAdapter(BadgesActivity.this, R.layout.adapter_spinner, spinnerAdapterData, getResources());

        spinner.setAdapter(adapter);
    }

    private void getAllCompetencyBadges(){
showLoading();
        APIRestClient.client(BadgesActivity.this).get("/api/IDP/getAllCompetencyBadges",null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                System.out.println("BADGES::: " + response);

                JSONArray jsonArray = null;
                Users user;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject badges = jsonArray.getJSONObject(i);

                        CompetencyBadges competencyBadges = CompetencyBadges.Builder.newInstance()
                                .setCompetencyId(badges.getString("CompetencyId"))
                                .setCompetency(badges.getString("Competency"))
                                .setRemarks(badges.getString("Remarks"))
                                .setBadgeType(badges.getString("BadgeType"))
                                .setBadgePoints(badges.getString("BadgePoints"))
                                .build();

                        competencyBadgesList.add(competencyBadges);
                    }

                    kConstants.setCompetencyBadges("competencyBadges", competencyBadgesList);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            }


            @Override
            public void onFinish() {

                super.onFinish();
hideLoading();
                adapter1 = new BadgesAdapter(BadgesActivity.this, competencyBadgesList);
                gridView.setAdapter(adapter1);
                adapter1.notifyDataSetChanged();

            }
        });

    }

    private void getAllCompetencyBadgesById(String userId){

showLoading();
        APIRestClient.client(BadgesActivity.this).get("/api/IDP/getBadgeProgress/userId/"+userId,null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                System.out.println("BADGES::: " + response);

                JSONArray jsonArray = null;
                Users user;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject badges = jsonArray.getJSONObject(i);

                        CompetencyBadges competencyBadges = CompetencyBadges.Builder.newInstance()
                                .setCompetencyId(badges.getString("CompetencyId"))
                            .setCompetency(badges.getString("CompetencyName"))
                            .setBadgeProgress((int)badges.getDouble("BadgeProgress"))
                            .setIsCompleted(badges.getBoolean("IsCompleted"))
                            .setBadgeType(badges.getString("CompetencyName"))
                                .build();

                        competencyBadgesList.add(competencyBadges);
                    }

                    kConstants.setCompetencyBadges("competencyBadges", competencyBadgesList);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("ERROR::::");
            }


            @Override
            public void onFinish() {

                super.onFinish();
                hideLoading();
                adapter1 = new BadgesAdapter(BadgesActivity.this, competencyBadgesList);
                gridView.setAdapter(adapter1);
                adapter1.notifyDataSetChanged();
            }
        });

    }

    private void getSparksBadges(){


        Users user;
        if(kConstants.getLoggedInUser() != null) {
            user = kConstants.getLoggedInUser();
        }
        else
        {
            return;
        }

        showLoading();
        APIRestClient.client(BadgesActivity.this).get("api/Sparks/getSparksBadgeByUserId/userId/"+kConstants.getLoggedInUser().UserId,null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                JSONArray jsonArray = null;
//                Users user;

                try {
                    jsonArray = new JSONArray(response);

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                        CompetencyBadges sparksBadges = CompetencyBadges.Builder.newInstance()
                                .setUserId(jsonObject.getInt("UserId"))
                                .setSparksId(jsonObject.getInt("SparksId"))
                                .setEventName(jsonObject.getString("EventName"))
                                .setIsClaimed(jsonObject.getBoolean("IsClaimed"))
                                .setQRCode(String.valueOf(jsonObject.getString("QRCode")))
                                .build();


                        System.out.println("SPARKS::::: " + sparksBadges.Competency);

                        sparksBadgesListById.add(sparksBadges);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("FAILED");
            }


            @Override
            public void onFinish() {

                super.onFinish();
hideLoading();
                adapter2 = new BadgesAdapter(BadgesActivity.this, sparksBadgesListById);
                gridViewSparks.setAdapter(adapter2);
                adapter2.notifyDataSetChanged();

            }
        });
    }

    private void showErrorMessage(String message){

        if(errorDialog != null){
            errorDialog.close();
        }

        Runnable function = new Runnable() {
            @Override
            public void run() {

            }
        };

        String title = "Message";
        errorDialog = new com.mbps.idp_journal.Helper_Dialog(this, title, message, function, null, true);
        errorDialog.setCancelable(false);
        errorDialog.show();
    }

    private void populateIndvidualizedBadges(){

        for (int i=0; i<10; i++){
            BadgesItem badge = new BadgesItem("Global Perspective" + i, R.drawable.badge_icon);
            individualizedBadgesList.add(badge);
        }
    }

    private void populateSparksBadges(){

        for (int i=0; i<10; i++){
            BadgesItem badge = new BadgesItem("Compelling Communication" + i, R.drawable.badge_icon);
            sparksBadgesList.add(badge);
        }
    }

}
