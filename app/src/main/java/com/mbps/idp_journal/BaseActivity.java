package com.mbps.idp_journal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.kaopiz.kprogresshud.KProgressHUD;

public class BaseActivity extends AppCompatActivity {
    Constants kConstants;

    private KProgressHUD progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        kConstants = new Constants(BaseActivity.this);

        if(progressDialog == null) {

            progressDialog = KProgressHUD.create(BaseActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    // .setDimAmount(5.0f)
                    .setCancellable(false)
                    .setLabel("Please wait")
                    .setMaxProgress(100)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }
    }


    public void showMenu(){

        Intent intentMain = new Intent(BaseActivity.this, MenuActivity.class);
        startActivity(intentMain);
    }


    public void showHome(){

        Intent intentMain = new Intent(BaseActivity.this, IDPJMainActivity.class);
        startActivity(intentMain);
        finish();
    }

    public void showLoading(){
        if(progressDialog == null) {

            progressDialog = KProgressHUD.create(BaseActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    // .setDimAmount(5.0f)
                    .setCancellable(false)
                    .setLabel("Please wait")
                    .setMaxProgress(100)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }
        progressDialog.show();
    }


    public void hideLoading(){
        if(progressDialog == null) {

            progressDialog = KProgressHUD.create(BaseActivity.this)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    // .setDimAmount(5.0f)
                    .setCancellable(false)
                    .setLabel("Please wait")
                    .setMaxProgress(100)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }
        progressDialog.dismiss();
    }


    public void dismissKeyboard(){

        findViewById(R.id.parentlayout).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                System.out.println("MOTION:::" + event.getAction());

                if(event.getAction() == MotionEvent.ACTION_UP && event.getAction() != MotionEvent.ACTION_MOVE){
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    return true;
                }

                return false;
            }
        });
    }
}
