package com.mbps.idp_journal;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mbps.idp_journal.api.APIRestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class LeaderboardsActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView leaderboardList, recyclerLeaderboardsSparks;
    LeaderboardsAdapter adapter, adapterSparks;
    TextView menuINDIVIDUALIZED, menuMonth, menuSPARKS;
    TextView textviewTop1;
    View viewMenuSelector;
    List<LeaderboardItem> rankList = new ArrayList<>();
    List<LeaderboardItem> rankListSparks = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboards);

        getLeaderBoardSparks();
        getLeaderBoardIndividualized();

        castViews();
        moveViewSelector(menuINDIVIDUALIZED);
    }

    public void castViews(){

        menuINDIVIDUALIZED = (TextView) findViewById(R.id.menuINDIVIDUALIZED);
        menuMonth = (TextView) findViewById(R.id.menu);
        menuSPARKS = (TextView) findViewById(R.id.menuSPARKS);
        textviewTop1 = (TextView) findViewById(R.id.textTop1);
        leaderboardList = (RecyclerView) findViewById(R.id.recyclerLeaderboards);
        viewMenuSelector = findViewById(R.id.viewMenuSelector);
        recyclerLeaderboardsSparks = findViewById(R.id.recyclerLeaderboardsSparks);

        menuINDIVIDUALIZED.setOnClickListener(this);
        menuMonth.setOnClickListener(this);
        menuSPARKS.setOnClickListener(this);

        leaderboardList.setVisibility(View.GONE);
        recyclerLeaderboardsSparks.setVisibility(View.GONE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaderboardsActivity.this);
        leaderboardList.setLayoutManager(mLayoutManager);
        leaderboardList.setItemAnimator(new DefaultItemAnimator());
        leaderboardList.setNestedScrollingEnabled(false);
        leaderboardList.setHasFixedSize(false);



        RecyclerView.LayoutManager mLayoutManagerSparks = new LinearLayoutManager(LeaderboardsActivity.this);
        recyclerLeaderboardsSparks.setLayoutManager(mLayoutManagerSparks);
        recyclerLeaderboardsSparks.setItemAnimator(new DefaultItemAnimator());
        recyclerLeaderboardsSparks.setNestedScrollingEnabled(false);
        recyclerLeaderboardsSparks.setHasFixedSize(false);

        ImageView menuButton = findViewById(R.id.menuButton);
        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
            }
        });

        ImageView homeButton = findViewById(R.id.homeButton);
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHome();
            }
        });

    }


    @Override
    public void onClick(View v) {
        if(v == menuINDIVIDUALIZED) {
            moveViewSelector(menuINDIVIDUALIZED);
//            updateIndividualizedAdapter();
            leaderboardList.setVisibility(View.VISIBLE);
            recyclerLeaderboardsSparks.setVisibility(View.GONE);

//            getLeaderBoardIndividualized();
        }
        else if(v == menuMonth) {
            moveViewSelector(menuMonth);
        }
        else if(v == menuSPARKS) {
            moveViewSelector(menuSPARKS);
            leaderboardList.setVisibility(View.GONE);
            recyclerLeaderboardsSparks.setVisibility(View.VISIBLE);

        }
    }

    public void moveViewSelector(View v) {

        RelativeLayout.LayoutParams p = (RelativeLayout.LayoutParams) viewMenuSelector.getLayoutParams();
        p.removeRule(RelativeLayout.ALIGN_BOTTOM);
        p.removeRule(RelativeLayout.ALIGN_START);
        p.removeRule(RelativeLayout.ALIGN_END);
        p.addRule(RelativeLayout.ALIGN_BOTTOM, v.getId());
        p.addRule(RelativeLayout.ALIGN_START, v.getId());
        p.addRule(RelativeLayout.ALIGN_END, v.getId());
        viewMenuSelector.setLayoutParams(p);
    }

    public void getLeaderBoardIndividualized(){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        showLoading();
        rankList = new ArrayList<>();
        APIRestClient.client(LeaderboardsActivity.this).get(/*"api/IDP/getLeaderBoardByDate/Month/" + (calendar.get(Calendar.MONTH) + 1) + "/Year/" + calendar.get(Calendar.YEAR)*/"/api/IDP/getLeaderBoard",null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);
                System.out.println("LEADERBOARDS::: " + response);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject leaders = jsonArray.getJSONObject(i);

                        LeaderboardItem leader = new LeaderboardItem.Builder().newInstance()
                                .setUsername(leaders.getString("UserName"))
                                .setEMail_Address(leaders.getString("EMail_Address"))
                                .setUserBadgePoints(leaders.getString("Point"))
                                .setAvatar(leaders.getString("ProfilePicture"))
                                .setUserRank(String.valueOf(i+1))
                                .build();

                        rankList.add(leader);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

                System.out.println("LEADERBOARDS::: " + new String(responseBody));
            }


            @Override
            public void onFinish() {

                super.onFinish();
hideLoading();
                adapter = new LeaderboardsAdapter(LeaderboardsActivity.this, rankList);
                leaderboardList.setAdapter(adapter);
                leaderboardList.setVisibility(View.VISIBLE);

            }
        });
    }

    public void getLeaderBoardSparks(){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        showLoading();
        APIRestClient.client(LeaderboardsActivity.this).get("api/Sparks/getLeaderBoardSparks",null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                System.out.println("LEADERBOARDS::: " + response);

                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject leaders = jsonArray.getJSONObject(i);

                        LeaderboardItem leader = new LeaderboardItem.Builder().newInstance()
                                .setUsername(leaders.getString("UserName"))
                                .setEMail_Address(leaders.getString("EMail_Address"))
                                .setUserBadgePoints(String.valueOf(leaders.getInt("BadgePoints")))
                                .setAvatar(String.valueOf(leaders.getString("ProfilePicture")))
                                .setUserRank(String.valueOf(i+1))
                                .build();

                        System.out.println("ITEM::: " + jsonArray);
                        rankListSparks.add(leader);

                    }

                } catch (JSONException e) {
                    System.out.println("ERROR::: " + e.getMessage());
                    e.printStackTrace();
                }

                hideLoading();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

                System.out.println("LEADERBOARDS FAILED::: " + new String(responseBody));
            }


            @Override
            public void onFinish() {

                super.onFinish();
hideLoading();

                adapterSparks = new LeaderboardsAdapter(LeaderboardsActivity.this, rankListSparks);
                recyclerLeaderboardsSparks.setAdapter(adapterSparks);

                System.out.println("LEADERBOARDS SPARKS::: " + rankListSparks.size());

            }
        });
    }
}
