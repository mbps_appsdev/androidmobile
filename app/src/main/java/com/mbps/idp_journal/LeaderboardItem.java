package com.mbps.idp_journal;

import org.json.JSONObject;

public class LeaderboardItem extends JSONObject {
     public String UserRank;
     public String UserAvatarPath;
     public String UserName;
     public String UserBadgePoints;
     public String EMail_Address;


     public LeaderboardItem(){

     }

    public LeaderboardItem(LeaderboardItem.Builder builder) {
        this.UserRank = builder.UserRank;
        this.UserAvatarPath = builder.UserAvatarPath;
        this.UserName = builder.UserName;
        this.UserBadgePoints = builder.UserBadgePoints;
        this.EMail_Address = builder.EMail_Address;
    }

    public static class Builder {
        private String UserRank;
        private String UserAvatarPath;
        private String UserName;
        private String UserBadgePoints;
        private String EMail_Address;

        public static LeaderboardItem.Builder newInstance(){
            return new LeaderboardItem.Builder();
        }

        public LeaderboardItem.Builder setUserRank(String UserRank){
            this.UserRank = UserRank;
            return this;
        }

        public LeaderboardItem.Builder setEMail_Address(String EMail_Address){
            this.EMail_Address = EMail_Address;
            return this;
        }

        public LeaderboardItem.Builder setAvatar(String UserAvatarPath){
            this.UserAvatarPath = UserAvatarPath;
            return this;
        }

        public LeaderboardItem.Builder setUsername(String UserName){
            this.UserName = UserName;
            return this;
        }

        public LeaderboardItem.Builder setUserBadgePoints(String UserBadgePoints){
            this.UserBadgePoints = UserBadgePoints;
            return this;
        }

        public LeaderboardItem build(){
            return new LeaderboardItem(this);
        }
    }
}








