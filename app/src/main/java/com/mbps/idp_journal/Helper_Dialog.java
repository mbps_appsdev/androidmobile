package com.mbps.idp_journal;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by jhec on 09/08/2017.
 */

public class Helper_Dialog extends Activity{

    Context dialogContext;
    String _title, _message, positivebutton_desc = "Yes", negativebutton_desc = "No";
    Runnable positiveButtonFunction;
    Runnable negativeButtonFunction;
    boolean okButtonOnly = false;

    Typeface tfB, tfR;

    static Dialog dialog;
    boolean cancelable_state = true;

    // Empty Constructor
    public Helper_Dialog() {

    }

    // Constructor for OK button only
    public Helper_Dialog(Context context, String title, String message) {
        dialogContext = context;
        _title = title;
        _message = message;
        okButtonOnly = true;
        setPositiveButton_Desc("OK");
    }

    // Constructor for Complete Parameter
    public Helper_Dialog(Context context, String title, String message,
                         final Runnable funcOfPositiveButton,
                         final Runnable funcOfNegativeButton, boolean isOkButtonOnly) {

        dialogContext = context;
        _title = title;
        _message = message;
        positiveButtonFunction = funcOfPositiveButton;
        negativeButtonFunction = funcOfNegativeButton;
        okButtonOnly = isOkButtonOnly;


        if (isOkButtonOnly) {
            setPositiveButton_Desc("OK");
        }

        tfB = Typeface.createFromAsset(dialogContext.getAssets(),Constants.FONT_BOLD);
        tfR = Typeface.createFromAsset(dialogContext.getAssets(),Constants.FONT_REGULAR);

    }

    // Constructor for OneLine Parameter
    public static  class PositiveButton {
        String buttonTitle;
        Runnable buttonAction;
        public PositiveButton(String title, Runnable action) {
            PositiveButton.this.buttonTitle = title;
            PositiveButton.this.buttonAction = action;
        }
    }

    public static class NegativeButton {
        String buttonTitle;
        Runnable buttonAction;
        public NegativeButton(String title, Runnable action) {
            NegativeButton.this.buttonTitle = title;
            NegativeButton.this.buttonAction = action;
        }
    }

    public Helper_Dialog(Context context,
                         final PositiveButton positiveButton,
                         final NegativeButton negativeButton,
                         String title, String message, boolean immediatelyShow) {

        this.dialogContext = context;
        this._title = title;
        this._message = message;
        if (positiveButton == null && negativeButton == null) {
            this.positivebutton_desc = "";
            this.positiveButtonFunction = null;
            this.negativebutton_desc = "";
            this.negativeButtonFunction = null;
            this.okButtonOnly = true;
        }
        else {
            this.positivebutton_desc = positiveButton.buttonTitle;
            this.positiveButtonFunction = positiveButton.buttonAction;
            this.negativebutton_desc = negativeButton.buttonTitle;
            this.negativeButtonFunction = negativeButton.buttonAction;
            this.okButtonOnly = false;
        }


        if (this.okButtonOnly) {
            setPositiveButton_Desc("OK");
        }

        this.tfB = Typeface.createFromAsset(dialogContext.getAssets(),Constants.FONT_BOLD);
        this.tfR = Typeface.createFromAsset(dialogContext.getAssets(),Constants.FONT_REGULAR);

        if (!immediatelyShow){
            return;
        }
        this.show();
    }


    public void setPositiveButton_Desc(String desc) {
        this.positivebutton_desc = desc;
    }

    public void setNoButton_Desc(String desc) {
        this.negativebutton_desc = desc;
    }

    public void setCancelable(boolean state) {
        cancelable_state = state;
    }

    public static void close() {
        dialog.dismiss();
    }

    public void show() {
        LayoutInflater factory = LayoutInflater.from(dialogContext);
        final View dialogView = factory.inflate(R.layout.alert_dialog, null);

        dialog = new Dialog(dialogContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        if (_title != null || _title.equals("")) {
            TextView lbl_title = (TextView) dialogView.findViewById(R.id.lbl_dialog_title);
            lbl_title.setText(_title);

            lbl_title.setTypeface(tfB, Typeface.NORMAL);
            lbl_title.setTextSize(TypedValue.COMPLEX_UNIT_PX,lbl_title.getTextSize());
        }

        if (_message != null || _message.equals("")) {
            TextView lbl_message = (TextView) dialogView.findViewById(R.id.lbl_dialog_message);
            lbl_message.setText(_message);

            lbl_message.setTypeface(tfR, Typeface.NORMAL);
            lbl_message.setTextSize(TypedValue.COMPLEX_UNIT_PX, lbl_message.getTextSize());
        }

        if (okButtonOnly == true) {
            dialogView.findViewById(R.id.btn_no).setVisibility(View.GONE);
            dialogView.findViewById(R.id.vw_divider).setVisibility(View.GONE);
            TextView btn_yes = (TextView) dialogView.findViewById(R.id.btn_yes);
            btn_yes.setText(positivebutton_desc);



            btn_yes.setTypeface(tfB, Typeface.NORMAL);
            btn_yes.setTextSize(TypedValue.COMPLEX_UNIT_PX,btn_yes.getTextSize());

            dialogView.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (positiveButtonFunction != null) {
                        positiveButtonFunction.run();
                    }

                }
            });

        } else {
            dialogView.findViewById(R.id.btn_no).setVisibility(View.VISIBLE);
            TextView btn_yes = (TextView) dialogView.findViewById(R.id.btn_yes);
            btn_yes.setText(positivebutton_desc);

            btn_yes.setTypeface(tfB, Typeface.NORMAL);
            btn_yes.setTextSize(TypedValue.COMPLEX_UNIT_PX,btn_yes.getTextSize());

            TextView btn_no = (TextView) dialogView.findViewById(R.id.btn_no);
            btn_no.setText(negativebutton_desc);

            btn_no.setTypeface(tfB, Typeface.NORMAL);
            btn_no.setTextSize(TypedValue.COMPLEX_UNIT_PX, btn_no.getTextSize());

            dialogView.findViewById(R.id.btn_yes)
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (positiveButtonFunction != null) {
                                positiveButtonFunction.run();
                            }
                        }
                    });
            dialogView.findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (negativeButtonFunction != null) {
                        negativeButtonFunction.run();
                    }
                }
            });
        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(this.cancelable_state);
        dialog.show();
    }

}
