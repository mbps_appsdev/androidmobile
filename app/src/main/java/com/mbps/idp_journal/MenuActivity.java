package com.mbps.idp_journal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MenuActivity extends BaseActivity {

    ImageView closeButton;
    TextView logout, name, email;
    ImageView profilePicture;
    RelativeLayout dashboard, badges, leaderboards, profilesearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        profilePicture = findViewById(R.id.imgProfilePic);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);

        name.setText(kConstants.getLoggedInUser().UserName);
        email.setText(kConstants.getLoggedInUser().EMail_Address);

        if(kConstants.getLoggedInUser() != null){

            switch (kConstants.getLoggedInUser().ProfilePicture)
            {
                case "agirl1":
                    profilePicture.setImageResource(R.drawable.agirl1);
                    break;

                case "agirl2":
                    profilePicture.setImageResource(R.drawable.agirl2);
                    break;

                case "agirl3":
                    profilePicture.setImageResource(R.drawable.agirl3);
                    break;

                case "agirl4":
                    profilePicture.setImageResource(R.drawable.agirl4);
                    break;

                case "agirl5":
                    profilePicture.setImageResource(R.drawable.agirl5);
                    break;

                case "agirl6":
                    profilePicture.setImageResource(R.drawable.agirl6);
                    break;

                case "aguy1":
                    profilePicture.setImageResource(R.drawable.aguy1);
                    break;

                case "aguy2":
                    profilePicture.setImageResource(R.drawable.aguy2);
                    break;

                case "aguy3":
                    profilePicture.setImageResource(R.drawable.aguy3);
                    break;

                case "aguy4":
                    profilePicture.setImageResource(R.drawable.aguy4);
                    break;

                case "aguy5":
                    profilePicture.setImageResource(R.drawable.aguy5);
                    break;

                case "aguy6":
                    profilePicture.setImageResource(R.drawable.aguy6);
                    break;

                default:
                    break;
            }

        }

        logout = (TextView) findViewById(R.id.logout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kConstants.setLoggedInUser(null);

                Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        castViews();

    }

    public void castViews(){
        closeButton = (ImageView) findViewById(R.id.closeButton);
        dashboard = (RelativeLayout) findViewById(R.id.layoutDashboard);
        badges = (RelativeLayout) findViewById(R.id.layoutBadges);
        leaderboards = (RelativeLayout) findViewById(R.id.layoutLeaderboards);
        profilesearch = (RelativeLayout) findViewById(R.id.layoutSearch);


        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMain = new Intent(MenuActivity.this, IDPJMainActivity.class);
                startActivity(intentMain);
                finish();
            }
        });

        leaderboards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentMain = new Intent(MenuActivity.this, LeaderboardsActivity.class);
                startActivity(intentMain);
                finish();
            }
        });

        badges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MenuActivity.this, BadgesActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
