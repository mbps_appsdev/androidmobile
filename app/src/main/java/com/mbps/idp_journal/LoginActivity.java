package com.mbps.idp_journal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mbps.idp_journal.api.APIRestClient;
import com.mbps.idp_journal.model.Users;

import org.json.JSONArray;
import org.json.JSONException;

public class LoginActivity extends BaseActivity {

    Button buttonLogin;
    Button buttonSignup;
    EditText editTextusername;
    EditText editTextpassword;
    Boolean isFirstLogin = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonLogin = (Button) findViewById(R.id.login);
        buttonSignup = (Button) findViewById(R.id.signup);

        editTextusername = (EditText) findViewById(R.id.username);
        editTextpassword = (EditText) findViewById(R.id.password);

        buttonLogin.setOnClickListener(loginListener);
        buttonSignup.setOnClickListener(loginListener);

        dismissKeyboard();

        if(isFirstLogin==false){
        }

        if(kConstants.getLoggedInUser() == null) {

        }
        else if(kConstants.getLoggedInUser().getClass() == Users.class) {
            isFirstLogin = false;
            hideLoading();
            Intent intentMain = new Intent(LoginActivity.this, IDPJMainActivity.class);
            startActivity(intentMain);
        }
    }

    private View.OnClickListener loginListener = new View.OnClickListener() {

        public void onClick(View v){
            switch(v.getId()){

                case R.id.login: {

                    String username = editTextusername.getText().toString();
                    String password = editTextpassword.getText().toString();

                    String parameter = "{" +
                            "  \"UserName\": \"username\"," +
                            "  \"Password\": \"password\"," +
                            "  \"DeviceId\": \"devicetest\"," +
                            "  \"DeviceOS\": \"android\"," +
                            "  \"RegistrationToken\": \"token\"" +
                            "}";

                    RequestParams params = new RequestParams();
                    params.put("UserName", username); //testuser
                    params.put("Password", password); //pass123
                    params.put("DeviceId", getDeviceId());
                    params.put("DeviceOS", "android");
                    params.put("RegistrationToken", getRegistrationToken());

                    System.out.println("PARAMS LOGIN::::::" + params);

                    showLoading();

                    APIRestClient.client(LoginActivity.this).post("/api/IDP/loginUser", params, new AsyncHttpResponseHandler() {

                        String response = "";

                        @Override
                        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                            response = new String(responseBody);
                            Users user;

                            System.out.println("LOGINRESPONSE:::: " + response);

                            try {

                                JSONArray jsonArray = new JSONArray(new String(response));
                                String userId = jsonArray.getJSONObject(0).get("UserId").toString();
                                String email = jsonArray.getJSONObject(0).get("EMail_Address").toString();
                                String status = jsonArray.getJSONObject(0).get("Status").toString();
                                String avatar = jsonArray.getJSONObject(0).get("ProfilePicture").toString();
                                String username = jsonArray.getJSONObject(0).get("UserName").toString();

                                user = new Users.Builder().newInstance()
                                            .setUserId(userId)
                                            .setEMail_Address(email)
                                            .setStatus(status)
                                            .setProfilePicture(avatar)
                                            .setUserName(username)
                                            .build();
                                Log.e("ERROR", "SUCCESSSSSS: " + user.UserId + " >>> " + user.EMail_Address);

                                    if(user.UserId != "0" && user.EMail_Address !=null){
                                        kConstants.setLoggedInUser(user);

                                        hideLoading();
                                        Intent intentMain = new Intent(LoginActivity.this, IDPJMainActivity.class);
                                        startActivity(intentMain);
                                    }
                                    else{
                                        AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setTitle(R.string.invalidcredentialheader);
                                        dialog.setMessage(R.string.invalidcredentialmessage);
                                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {

                                            }
                                        });
                                        final AlertDialog alert = dialog.create();
                                        alert.show();

                                        hideLoading();
                                    }

                            } catch (JSONException e) {
                                e.printStackTrace();

                                AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                dialog.setCancelable(false);
                                dialog.setTitle(R.string.invalidcredentialheader);
                                dialog.setMessage(R.string.invalidcredentialmessage);
                                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                final AlertDialog alert = dialog.create();
                                alert.show();
                            }

                        }


                        @Override
                        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                            System.out.println("FAILED " + statusCode);
                            hideLoading();
                            if(statusCode == 403){
                                AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                dialog.setCancelable(false);
                                String res = new String(responseBody);

                                dialog.setTitle(((new Gson().fromJson(res, JsonObject.class)).get("Message").toString()));
                                dialog.setMessage(((new Gson().fromJson(res, JsonObject.class)).get("Message").toString()));
                                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                final AlertDialog alert = dialog.create();
                                alert.show();
                            }
                            else {

                                AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
                                dialog.setCancelable(false);
                                dialog.setTitle(R.string.invalidcredentialheader);
                                dialog.setMessage(R.string.invalidcredentialmessage);
                                dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                });
                                final AlertDialog alert = dialog.create();
                                alert.show();

                            }
                        }


                        @Override
                        public void onFinish() {

                            super.onFinish();

                            hideLoading();
                        }
                    });
                }
                    break;

                case R.id.signup:
                    Intent intentLogin = new Intent(LoginActivity.this, SignupActivity.class);
                    startActivity(intentLogin);
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {

    }


    private String getRegistrationToken(){
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        return refreshedToken;
        return "token";
    }


    private String getDeviceId(){
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return  android_id;
    }

}
