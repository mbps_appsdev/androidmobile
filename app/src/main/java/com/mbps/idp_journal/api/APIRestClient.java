package com.mbps.idp_journal.api;

import android.content.Context;
import android.util.Log;

import com.kaopiz.kprogresshud.KProgressHUD;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mbps.idp_journal.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;


public class APIRestClient {
    private static final String BASE_URL = Constants.API_BASE;

    private static AsyncHttpClient client;

    private static KProgressHUD progressDialog;

    private static APIRestClient self;

    private static Context context;

    public APIRestClient(){
        self = this;
    }

    public static APIRestClient client(Context ctx){
        context = ctx;

        if(self == null) {
            self = new APIRestClient();
        }

        if(client == null) {
            client = new AsyncHttpClient();
        }

        if(progressDialog == null) {
            progressDialog = KProgressHUD.create(ctx)
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                    // .setDimAmount(5.0f)
                    .setCancellable(false)
                    .setLabel("Please wait")
                    .setMaxProgress(100)
                    .setAnimationSpeed(2)
                    .setDimAmount(0.5f);
        }

        return self;
    }

    public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

//        progressDialog.show();

        if(NetworkHelper.isInternetAvailable(context)) {

            Log.v("log", "GET: " + getAbsoluteUrl(url));
            client.get(getAbsoluteUrl(url), params, responseHandler);

            //progressDialog.dismiss();
        }
    }

    public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

//        progressDialog.show();

        if(NetworkHelper.isInternetAvailable(context)) {

            Log.v("log", "POST: " + getAbsoluteUrl(url));
            client.post(getAbsoluteUrl(url), params, responseHandler);
            //progressDialog.dismiss();
        }
    }


    public void put(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {

        //progressDialog.show();

        if(NetworkHelper.isInternetAvailable(context)) {

            Log.v("log", "PUT: " + getAbsoluteUrl(url));
            client.put(getAbsoluteUrl(url), params, responseHandler);
            //progressDialog.dismiss();
        }
    }


    public void postJSONARRAY(String url, JSONArray params, AsyncHttpResponseHandler responseHandler) {

//        progressDialog.show();

        if(NetworkHelper.isInternetAvailable(context)) {

            StringEntity se = null;
            try {
                se = new StringEntity(params.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return;
            }


            Log.e("ERROR", "response: " + params.toString());

            Log.v("log", "POST: " + getAbsoluteUrl(url));
            client.post(context, getAbsoluteUrl(url), se, "application/json", responseHandler);
            //progressDialog.dismiss();
        }
    }

    public void putJSONOBJ(String url, JSONObject params, AsyncHttpResponseHandler responseHandler) {

       // progressDialog.show();

        if(NetworkHelper.isInternetAvailable(context)) {

            StringEntity se = null;
            try {
                se = new StringEntity(params.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return;
            }


            Log.e("ERROR", "response: " + params.toString());

            Log.v("log", "POST: " + getAbsoluteUrl(url));
            client.put(context, getAbsoluteUrl(url), se, "application/json", responseHandler);
            //progressDialog.dismiss();
        }
    }

    private String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }


    public void showProgress(){
        progressDialog.show();
    }


    public void dismissProgress(){
        progressDialog.dismiss();
    }
}
