package com.mbps.idp_journal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mbps.idp_journal.model.CompetencyBadges;

import java.util.ArrayList;

public class BadgesAdapter extends BaseAdapter {

    ArrayList<CompetencyBadges> badges;
    Context context;
    LayoutInflater layoutInflater;

    public BadgesAdapter(Context context, ArrayList<CompetencyBadges> badges){
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.badges = badges;
    }

    @Override
    public int getCount() {
        return badges.size();
    }

    @Override
    public Object getItem(int position) {
        return badges.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){

            convertView = layoutInflater.inflate(R.layout.badge_layout, parent, false);

            TextView badgeItem = (TextView) convertView.findViewById(R.id.textBadge);
            ImageView badgeImage = (ImageView) convertView.findViewById(R.id.imgBadge);

            if(badges.get(position).CompetencyId!=null){
                System.out.println("COMPETENCY::: " + badges.get(position).Competency);

                badgeItem.setText(badges.get(position).Competency);



                switch (badges.get(position).BadgeType){
                    case "Global Perspective":
                        badgeImage.setImageResource(R.drawable.globalperspective);
                        break;

                    case "Collaboration":
                        badgeImage.setImageResource(R.drawable.collaboration);
                        break;

                    case "Compelling Communication":
                        badgeImage.setImageResource(R.drawable.compellingcommunication);
                        break;

                    case "Influence and Persuasion":
                        badgeImage.setImageResource(R.drawable.influenceandpersuasion);
                        break;

                    case "Innovative Mindset":
                        badgeImage.setImageResource(R.drawable.innovativemindset);
                        break;

                    case "Adaptability":
                        badgeImage.setImageResource(R.drawable.adaptability);
                        break;

                    case "Knowing the Business":
                        badgeImage.setImageResource(R.drawable.knowingthebusiness);
                        break;

                    case "Technology Savvy":
                        badgeImage.setImageResource(R.drawable.technologysavvy);
                        break;

                    case "Executing with Excellence":
                        badgeImage.setImageResource(R.drawable.executingwithexcellence);
                        break;

                    case "Continuous Self-Improvement":
                        badgeImage.setImageResource(R.drawable.continuousselfimprovement);
                        break;

                    case "Solution Focused":
                        badgeImage.setImageResource(R.drawable.solutionfocused);
                        break;

                    case "Building and Leading Teams":
                        badgeImage.setImageResource(R.drawable.buildingandleadingteams);
                        break;

                    case "Coaching and Developing":
                        badgeImage.setImageResource(R.drawable.coachinganddeveloping);
                        break;

                    case "Inspiring Commitment":
                        badgeImage.setImageResource(R.drawable.inspiring);
                        break;

                    case "Strategic Visioning":
                        badgeImage.setImageResource(R.drawable.strategicvisioning);
                        break;

                    case "Talent Management":
                        badgeImage.setImageResource(R.drawable.talentmanagement);
                        break;
                }

                badgeImage.setAlpha((float) 0.4);

                System.out.println("IsCompleted:::: " + badges.get(position).Competency + " - " + badges.get(position).IsCompleted);

                if(badges.get(position).IsCompleted == true) {
                    badgeImage.setAlpha((float) 1.0);
                }
            }
            else {
                badgeItem.setText(badges.get(position).EventName);

                switch (badges.get(position).EventName){

                    case "Agile – New Ways of Working":
                        badgeImage.setImageResource(R.drawable.agile);
                        break;

                    case "Power BI":
                        badgeImage.setImageResource(R.drawable.powerbi);
                        break;

                    case "Emotional Intelligence: A Critical Skill for Leaders during Transformation":
                        badgeImage.setImageResource(R.drawable.emotionalintelligenceforleaders);
                        break;

                    case "Network Mapping":
                        badgeImage.setImageResource(R.drawable.networking);
                        break;

                    case "Digital Leadership Mindset":
                        badgeImage.setImageResource(R.drawable.digitalleadershipmindset);
                        break;

                    case "Communicating as a Leader Training":
                        badgeImage.setImageResource(R.drawable.influencingskills);
                        break;

                    case "Obsess about customers":
                        badgeImage.setImageResource(R.drawable.obsessaboutcustomers);
                        break;

                    case "Think big":
                        badgeImage.setImageResource(R.drawable.thinkbig);
                        break;

                    case "Do the right thing":
                        badgeImage.setImageResource(R.drawable.dotherightthing);
                        break;

                    case "Own it":
//                        badgeImage.setImageResource(R.drawable.ownit);
                        break;

                    case "Get it done together":
                        badgeImage.setImageResource(R.drawable.getitdonetogether);
                        break;

                    case "Share your humanity":
//                        badgeImage.setImageResource(R.drawable.shareyourhumanity);
                        break;
                }

                badgeImage.setAlpha((float) 0.4);

                if(badges.get(position).IsClaimed == true) {
                    badgeImage.setAlpha((float) 1.0);
                }
            }



        }


        return convertView;
    }


}