package com.mbps.idp_journal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mbps.idp_journal.model.Notifications;

import java.util.ArrayList;

public class NotificationsActivity extends AppCompatActivity {

    ImageView closeButton;
    RecyclerView recyclerView;
    NotificationsAdapter notificationsAdapter;
    ArrayList<Notifications> notificationLists = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        recyclerView = findViewById(R.id.recycler);
        closeButton = findViewById(R.id.closeButton);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        populateNotifications();

        notificationsAdapter = new NotificationsAdapter(NotificationsActivity.this, notificationLists);
        recyclerView.setAdapter(notificationsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

    }

    private void populateNotifications() {

        for (int i = 0; i < 10; i++) {

            boolean isHeader;

            if(i%2==0){
                isHeader = true;
            }
            else{
                isHeader = false;
            }

            Notifications notification = Notifications.Builder.newInstance()
                    .setNotificationsTitle("It's Your Birthday! - " + i)
                    .setNotificationsDetails("Hello It's Your Birthday! - " + i)
                    .setIsHeader(isHeader)
                    .build();

            notificationLists.add(notification);
        }
    }
}
