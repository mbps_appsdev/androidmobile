package com.mbps.idp_journal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Helper_Font {

    public static Typeface tfR;
    public static Typeface tfB;

    // Constructor use for activity
    public void setFont(ViewGroup group, Activity current_activity) {

        tfB = Typeface.createFromAsset(current_activity.getAssets(), Constants.FONT_BOLD);
        tfR = Typeface.createFromAsset(current_activity.getAssets(), Constants.FONT_REGULAR);

        int count = group.getChildCount();



        View v;
        for (int i = 0; i < count; i++) {

            v = group.getChildAt(i);
            if (v instanceof TextView) {
                if (v.getClass() == TextView.class) {

                    TextView tv = (TextView) v;

                    if (tv.getTypeface() != null) {
                        if ((tv.getTypeface().getStyle() == Typeface.BOLD)) {
                            tv.setTypeface(tfB, Typeface.NORMAL);
                        } else{
                            tv.setTypeface(tfR);
                        }
                    } else {
                        tv.setTypeface(tfR);
                    }
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, tv.getTextSize());

                } else if (v.getClass() == Button.class) {
                    Button btn = (Button) v;

                    if (btn.getTypeface() != null) {
                        if ((btn.getTypeface().getStyle() == Typeface.BOLD)) {
                            btn.setTypeface(tfB, Typeface.NORMAL);
                        } else {
                            btn.setTypeface(tfR);
                        }
                    } else {
                        btn.setTypeface(tfR);
                    }
                    btn.setTextSize(TypedValue.COMPLEX_UNIT_PX,btn.getTextSize());
                } else if (v.getClass() == EditText.class) {
                    EditText txt = (EditText) v;
                    if (txt.getTypeface() != null) {
                        if ((txt.getTypeface().getStyle() == Typeface.BOLD)) {
                            txt.setTypeface(tfB, Typeface.NORMAL);
                        } else {
                            txt.setTypeface(tfR);
                        }
                    } else {
                        txt.setTypeface(tfR);
                    }
                    txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, txt.getTextSize());
                }
            } else if (v instanceof ViewGroup)

                setFont((ViewGroup) v, current_activity);
        }
    }


    public void setFont(ViewGroup group, Context app_context) {

        tfB = Typeface.createFromAsset(app_context.getAssets(), Constants.FONT_BOLD);
        tfR = Typeface.createFromAsset(app_context.getAssets(), Constants.FONT_REGULAR);

        int count = group.getChildCount();

        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView) {
                TextView tv = (TextView) v;

                if (tv.getTypeface() != null) {
                    if ((tv.getTypeface().getStyle() == Typeface.BOLD)) {
                        tv.setTypeface(tfB, Typeface.NORMAL);
                    } else{
                        tv.setTypeface(tfR);
                    }
                } else {
                    tv.setTypeface(tfR);
                }
                tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, tv.getTextSize());

            } else if (v instanceof Button) {
                Button btn = (Button) v;
                if (btn.getTypeface() != null) {
                    if ((btn.getTypeface().getStyle() == Typeface.BOLD)) {
                        btn.setTypeface(tfB, Typeface.NORMAL);
                    } else {
                        btn.setTypeface(tfR);
                    }
                } else {
                    btn.setTypeface(tfR);
                }
                btn.setTextSize(TypedValue.COMPLEX_UNIT_PX,btn.getTextSize());

            } else if (v instanceof EditText) {
                EditText txt = (EditText) v;
                if (txt.getTypeface() != null) {
                    if ((txt.getTypeface().getStyle() == Typeface.BOLD)) {
                        txt.setTypeface(tfB, Typeface.NORMAL);
                    } else {
                        txt.setTypeface(tfR);
                    }
                } else {
                    txt.setTypeface(tfR);
                }
                txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, txt.getTextSize());
            } else if (v instanceof ViewGroup)
                setFont((ViewGroup) v, app_context);
        }
    }
}
