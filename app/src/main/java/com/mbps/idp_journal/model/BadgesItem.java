package com.mbps.idp_journal.model;

public class BadgesItem {

    String item;
    int image;

    public BadgesItem(){
        this.item = "";
    }

    public BadgesItem(String item, int image){
        this.item = item;
        this.image = image;
    }

    public String getBadgeItem(){
        return item;
    }

    public int getBadgeImage(){
        return image;
    }

}
