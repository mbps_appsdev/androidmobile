package com.mbps.idp_journal.model;

import org.json.JSONObject;

public class Notifications extends JSONObject implements ListItem {

    public String NotificationsTitle;
    public String NotificationsDetails;
    public boolean IsHeader;

    public Notifications(Builder builder){
        this.NotificationsTitle = builder.Notificationstitle;
        this.NotificationsDetails = builder.NotificationsDetails;
        this.IsHeader = builder.IsHeader;
    }

    @Override
    public boolean isHeader() {
        return this.IsHeader;
    }

    @Override
    public Notifications getNotification() {
        return this;
    }


    public static class Builder{

        private String Notificationstitle;
        private String NotificationsDetails;
        private Boolean IsHeader;

        public static Builder newInstance(){

            return new Builder();
        }

        public Notifications.Builder setNotificationsTitle(String NotificationsTitle){
            this.Notificationstitle = NotificationsTitle;
            return this;
        }

        public Notifications.Builder setNotificationsDetails(String NotificationsDetails){
            this.NotificationsDetails = NotificationsDetails;
            return this;
        }

        public Notifications.Builder setIsHeader(Boolean IsHeader){
            this.IsHeader = IsHeader;
            return this;
        }

        public Notifications build(){
            return new Notifications(this);
        }
    }
}
