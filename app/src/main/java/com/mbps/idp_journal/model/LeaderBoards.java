package com.mbps.idp_journal.model;

import org.json.JSONObject;

public class LeaderBoards extends JSONObject {
    public String UserName;
    public String EMail_Address;
    public String Points;

    public LeaderBoards(LeaderBoards.Builder builder){
        this.UserName = builder.UserName;
        this.EMail_Address = builder.EMail_Address;
        this.Points = builder.Points;
    }

    public static class Builder {
        private String UserName;
        private String EMail_Address;
        private String Points;

        public static LeaderBoards.Builder newInstance(){
            return new LeaderBoards.Builder();
        }

        public LeaderBoards.Builder setUserName(String UserName){
            this.UserName = UserName;
            return this;
        }

        public LeaderBoards.Builder setEMail_Address(String EMail_Address){
            this.EMail_Address = EMail_Address;
            return this;
        }

        public LeaderBoards.Builder setPoints(String Points){
            this.Points = Points;
            return this;
        }

        public LeaderBoards build(){
            return new LeaderBoards(this);
        }
    }
}
