package com.mbps.idp_journal.model;

import org.json.JSONObject;

public class Users extends JSONObject {

    public String UserId;
    public String EMail_Address;
    public String Status;
    public String ProfilePicture;
    public String UserName;

    public Users(Builder builder){
        this.UserId = builder.UserId;
        this.EMail_Address = builder.EMail_Address;
        this.Status = builder.Status;
        this.ProfilePicture = builder.ProfilePicture;
        this.UserName = builder.UserName;
    }

    public static class Builder {
        private String UserId;
        private String EMail_Address;
        private String Status;
        private String ProfilePicture;
        private String UserName;

        public static Builder newInstance(){
            return new Builder();
        }

        public Builder setUserId(String UserId){
            this.UserId = UserId;
            return this;
        }

        public Builder setEMail_Address(String EMail_Address){
            this.EMail_Address = EMail_Address;
            return this;
        }

        public Builder setStatus(String Status){
            this.Status = Status;
            return this;
        }

        public Builder setProfilePicture(String ProfilePicture){
            this.ProfilePicture = ProfilePicture;
            return this;
        }

        public Builder setUserName(String UserName){
            this.UserName = UserName;
            return this;
        }

        public Users build(){
            return new Users(this);
        }
    }
}
