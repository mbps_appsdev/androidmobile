package com.mbps.idp_journal.model;

public class SparksBadges {

    public int UserId;
    public int SparksId;
    public String EventName;
    public Boolean IsClaimed;
    public String QRCode;

    public SparksBadges(){

    }

    public SparksBadges(int UserId, int SparksId, String EventName, Boolean IsClaimed, String QRCode){
        this.UserId = UserId;
        this.SparksId = SparksId;
        this.EventName = EventName;
        this.IsClaimed = IsClaimed;
        this.QRCode = QRCode;
    }

    public int getUserId(){
        return this.UserId;
    }

    public int getSparksId(){
        return this.SparksId;
    }

    public String getEventName(){
        return EventName;
    }

    public Boolean IsClaimed(){
        return this.IsClaimed;
    }

    public String getQRCode(){
        return QRCode;
    }
}
