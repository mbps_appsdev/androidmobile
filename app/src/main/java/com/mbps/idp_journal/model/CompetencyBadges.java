package com.mbps.idp_journal.model;

public class CompetencyBadges {

    public String CompetencyId;
    public String Competency;
    public String Remarks;
    public String BadgeType;
    public String BadgePoints;
    public int BadgeProgress;
    public boolean IsCompleted;
    public int UserId;
    public int SparksId;
    public String EventName;
    public boolean IsClaimed;
    public String QRCode;

    public CompetencyBadges(CompetencyBadges.Builder builder){
        this.CompetencyId = builder.CompetencyId;
        this.Competency = builder.Competency;
        this.Remarks = builder.Remarks;
        this.BadgeType = builder.BadgeType;
        this.BadgePoints = builder.BadgePoints;
        this.BadgeProgress = builder.BadgeProgress;
        this.IsCompleted = builder.IsCompleted;
        this.UserId = builder.UserId;
        this.SparksId = builder.SparksId;
        this.EventName = builder.EventName;
        this.IsClaimed = builder.IsClaimed;
        this.QRCode = builder.QRCode;
    }

    public static class Builder {
        private String CompetencyId;
        private String Competency;
        private String Remarks;
        private String BadgeType;
        private String BadgePoints;
        private int BadgeProgress;
        private boolean IsCompleted;
        private int UserId;
        private int SparksId;
        private String EventName;
        private boolean IsClaimed;
        private String QRCode;

        public static CompetencyBadges.Builder newInstance(){
            return new CompetencyBadges.Builder();
        }

        public CompetencyBadges.Builder setCompetencyId(String CompetencyId){
            this.CompetencyId = CompetencyId;
            return this;
        }

        public CompetencyBadges.Builder setCompetency(String Competency){
            this.Competency = Competency;
            return this;
        }

        public CompetencyBadges.Builder setRemarks(String Remarks){
            this.Remarks = Remarks;
            return this;
        }

        public CompetencyBadges.Builder setBadgeType(String BadgeType){
            this.BadgeType = BadgeType;
            return this;
        }

        public CompetencyBadges.Builder setBadgePoints(String BadgePoints){
            this.BadgePoints = BadgePoints;
            return this;
        }

        public CompetencyBadges.Builder setIsCompleted(boolean IsCompleted){
            this.IsCompleted = IsCompleted;
            return this;
        }

        public CompetencyBadges.Builder setBadgeProgress(int BadgeProgress){
            this.BadgeProgress = BadgeProgress;
            return this;
        }

        public CompetencyBadges.Builder setUserId(int UserId){
            this.UserId = UserId;
            return this;
        }

        public CompetencyBadges.Builder setSparksId(int SparksId){
            this.SparksId = SparksId;
            return this;
        }

        public CompetencyBadges.Builder setEventName(String EventName){
            this.EventName = EventName;
            return this;
        }

        public CompetencyBadges.Builder setIsClaimed(Boolean IsClaimed){
            this.IsClaimed = IsClaimed;
            return this;
        }

        public CompetencyBadges.Builder setQRCode(String QRCode){
            this.QRCode = QRCode;
            return this;
        }

        public CompetencyBadges build(){
            return new CompetencyBadges(this);
        }
    }
}
