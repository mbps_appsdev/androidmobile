package com.mbps.idp_journal.model;

public interface ListItem {
    boolean isHeader();
    Notifications getNotification();
}
