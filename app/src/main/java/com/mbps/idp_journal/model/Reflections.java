package com.mbps.idp_journal.model;

import org.json.JSONObject;

public class Reflections extends JSONObject {

        public String IDP_ReflectionId;
        public String IDPId;
        public String Year;
        public String Month;
        public String ConfigId;
        public String ConfigValue;
        public String ReflectionType;
        public String Reflection;
        public String CreatedBy;
        public String CreatedDate;
        public String ModifiedBy;
        public String ModifiedDate;

    public Reflections(Reflections.Builder builder){
        this.IDP_ReflectionId = builder.IDP_ReflectionId;
        this.IDPId = builder.IDPId;
        this.Year = builder.Year;
        this.Month = builder.Month;
        this.ConfigId = builder.ConfigId;
        this.ConfigValue = builder.ConfigValue;
        this.ReflectionType = builder.ReflectionType;
        this.Reflection = builder.Reflection;
        this.CreatedBy = builder.CreatedBy;
        this.CreatedDate = builder.CreatedDate;
        this.ModifiedBy = builder.ModifiedBy;
        this.ModifiedDate = builder.ModifiedDate;
    }

    public static class Builder {
        private String IDP_ReflectionId;
        private String IDPId;
        private String Year;
        private String Month;
        private String ConfigId;
        private String ConfigValue;
        private String ReflectionType;
        private String Reflection;
        private String CreatedBy;
        private String CreatedDate;
        private String ModifiedBy;
        private String ModifiedDate;

        public static Reflections.Builder newInstance(){
            return new Reflections.Builder();
        }

        public Reflections.Builder setIDP_ReflectionId(String IDP_ReflectionId){
            this.IDP_ReflectionId = IDP_ReflectionId;
            return this;
        }

        public Reflections.Builder setIDPId(String IDPId){
            this.IDPId = IDPId;
            return this;
        }

        public Reflections.Builder setYear(String Year){
            this.Year = Year;
            return this;
        }

        public Reflections.Builder setMonth(String Month){
            this.Month = Month;
            return this;
        }

        public Reflections.Builder setConfigId(String ConfigId){
            this.ConfigId = ConfigId;
            return this;
        }

        public Reflections.Builder setConfigValue(String ConfigValue){
            this.ConfigValue = ConfigValue;
            return this;
        }

        public Reflections.Builder setReflectionType(String ReflectionType){
            this.ReflectionType = ReflectionType;
            return this;
        }

        public Reflections.Builder setReflection(String Reflection){
            this.Reflection = Reflection;
            return this;
        }

        public Reflections.Builder setCreatedBy(String CreatedBy){
            this.CreatedBy = CreatedBy;
            return this;
        }

        public Reflections.Builder setCreatedDate(String CreatedDate){
            this.CreatedDate = CreatedDate;
            return this;
        }

        public Reflections.Builder setModifiedBy(String ModifiedBy){
            this.ModifiedBy = ModifiedBy;
            return this;
        }

        public Reflections.Builder setModifiedDate(String ModifiedDate){
            this.ModifiedDate = ModifiedDate;
            return this;
        }

        public Reflections build(){
            return new Reflections(this);
        }
    }
}
