package com.mbps.idp_journal.model;

public class ReflectionItem {
    public int Year;
    public int Month;
    public String ReflectionType;
    public int CreatedBy;
    public int ModifiedBy;

    public ReflectionItem(){
        this.Year = 0;
        this.Month = 0;
        this.ReflectionType = "";
        this.CreatedBy = 0;
        this.ModifiedBy = 0;
    }
}
