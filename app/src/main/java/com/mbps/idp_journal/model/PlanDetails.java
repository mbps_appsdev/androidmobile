package com.mbps.idp_journal.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PlanDetails extends JSONObject implements Comparable<PlanDetails> {

    public String IDPId;
    public String CompetencyId;
    public String Competency;
    public String Description;
    public String ActionPlan;
    public String Support;
    public String TargetCompletionDate;
    public String ActualCompletionDate;
    public String CreatedBy;
    public String CreatedDate;
    public String ModifiedBy;
    public String ModifiedDate;
    public ArrayList<SuccessMeasure> SuccessMeasure;
    public int OverallProgress;
    public String Remarks;
    public Boolean IsCompleted;

    public PlanDetails(PlanDetails.Builder builder){
        this.IDPId = builder.IDPId;
        this.CompetencyId = builder.CompetencyId;
        this.Competency = builder.Competency;
        this.Description = builder.Description;
        this.ActionPlan = builder.ActionPlan;
        this.Support = builder.Support;
        this.TargetCompletionDate = builder.TargetCompletionDate;
        this.ActualCompletionDate = builder.ActualCompletionDate;
        this.CreatedBy = builder.CreatedBy;
        this.CreatedDate = builder.CreatedDate;
        this.ModifiedBy = builder.ModifiedBy;
        this.ModifiedDate = builder.ModifiedDate;
        this.SuccessMeasure = builder.SuccessMeasure;
        this.OverallProgress = builder.OverallProgress;
        this.Remarks = builder.Remarks;
        this.IsCompleted = builder.IsCompleted;
    }


    @Override
    public int compareTo(PlanDetails o) {

        try {

            SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-DD'T'HH:mm:ss");
            Date date1 = df.parse(this.CreatedDate);
            Date date2 = df.parse(o.CreatedDate);
            return date1.compareTo(date2);
        }
        catch (Exception e) {
            return 0;
        }
    }

    public static class Builder {
        private String IDPId;
        private String CompetencyId;
        private String Competency;
        private String Description;
        private String ActionPlan;
        private String Support;
        private String TargetCompletionDate;
        private String ActualCompletionDate;
        private String CreatedBy;
        private String CreatedDate;
        private String ModifiedBy;
        private String ModifiedDate;
        private ArrayList<SuccessMeasure> SuccessMeasure;
        private int OverallProgress;
        private String Remarks;
        private Boolean IsCompleted;


        public static PlanDetails.Builder newInstance(){
            return new PlanDetails.Builder();
        }

        public PlanDetails.Builder setIDPId(String IDPId){
            this.IDPId = IDPId;
            return this;
        }

        public PlanDetails.Builder setCompetencyId(String CompetencyId){
            this.CompetencyId = CompetencyId;
            return this;
        }

        public PlanDetails.Builder setCompetency(String Competency){
            this.Competency = Competency;
            return this;
        }

        public PlanDetails.Builder setDescription(String Description){
            this.Description = Description;
            return this;
        }

        public PlanDetails.Builder setActionPlan(String ActionPlan){
            this.ActionPlan = ActionPlan;
            return this;
        }

        public PlanDetails.Builder setSupport(String Support){
            this.Support = Support;
            return this;
        }

        public PlanDetails.Builder setTargetCompletionDate(String TargetCompletionDate){
            this.TargetCompletionDate = TargetCompletionDate;
            return this;
        }

        public PlanDetails.Builder setActualCompletionDate(String ActualCompletionDate){
            this.ActualCompletionDate = ActualCompletionDate;
            return this;
        }

        public PlanDetails.Builder setCreatedBy(String CreatedBy){
            this.CreatedBy = CreatedBy;
            return this;
        }

        public PlanDetails.Builder setCreatedDate(String CreatedDate){
            this.CreatedDate = CreatedDate;
            return this;
        }

        public PlanDetails.Builder setModifiedBy(String ModifiedBy){
            this.ModifiedBy = ModifiedBy;
            return this;
        }

        public PlanDetails.Builder setModifiedDate(String ModifiedDate){
            this.ModifiedDate = ModifiedDate;
            return this;
        }

        public PlanDetails.Builder setOverallProgress(int OverallProgress){
            this.OverallProgress = OverallProgress;
            return this;
        }

        public PlanDetails.Builder setRemarks(String Remarks){
            this.Remarks = Remarks;
            return this;
        }

        public PlanDetails.Builder setIsCompleted(boolean IsCompleted){
            this.IsCompleted = IsCompleted;
            return this;
        }

        public PlanDetails.Builder setSuccessMeasure(JSONArray successMeasure){

            Log.e("ERROR", "successMeasure: " + successMeasure);
            this.SuccessMeasure = new ArrayList<>();

            for(int i = 0; i < successMeasure.length(); i++) {

                try {

                    JSONObject jsonObject = (JSONObject) successMeasure.get(i);

                    Log.e("ERROR", "object: " + jsonObject);

                    SuccessMeasure measure = new SuccessMeasure(jsonObject.getInt("IDP_MeasurementId"), jsonObject.getInt("IDP_Id"), jsonObject.getString("Measure"), jsonObject.getBoolean("IsCompleted"));

                    this.SuccessMeasure.add(measure);
                }
                catch (Exception e) {
                    Log.e("ERROR", "exception: " + e.getMessage());
                }
            }
//            this.SuccessMeasure = SuccessMeasure;
            return this;
        }

        public PlanDetails build(){
            return new PlanDetails(this);
        }
    }

}
