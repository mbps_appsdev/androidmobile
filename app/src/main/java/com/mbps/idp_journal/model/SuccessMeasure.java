package com.mbps.idp_journal.model;

public class SuccessMeasure {

    public int IDP_MeasurementId;
    public int IDP_Id;
    public String Measure;
    public Boolean IsCompleted;

    public SuccessMeasure(){

    }

    public SuccessMeasure(Integer id, Integer idp, String measure, Boolean completed) {
        this.IDP_MeasurementId = id;
        this.IDP_Id = idp;
        this.Measure = measure;
        this.IsCompleted = completed;
    }
}
