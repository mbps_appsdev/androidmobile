package com.mbps.idp_journal;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mbps.idp_journal.model.CompetencyBadges;

import java.util.ArrayList;

public class CompetencyAdapter extends ArrayAdapter<String> {

    private ArrayList<CompetencyBadges> mData;
    public Resources mResources;
    private LayoutInflater mInflater;


    public CompetencyAdapter(
            Activity activitySpinner,
            int textViewResourceId,
            ArrayList objects,
            Resources resLocal
    ) {
        super(activitySpinner, textViewResourceId, objects);

        mData = objects;
        mResources = resLocal;
        mInflater = (LayoutInflater) activitySpinner.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = mInflater.inflate(R.layout.competency_spinner, parent, false);
        TextView item = (TextView) row.findViewById(R.id.item);

        CompetencyBadges competencyBadges = mData.get(position);

        item.setText(competencyBadges.Competency.toString());


        //Set meta data here and later we can access these values from OnItemSelected Event Of Spinner
//        row.setTag(R.string.meta_position, Integer.toString(position));
//        row.setTag(R.string.meta_title, mData.get(position).toString());

        return row;
    }
}
