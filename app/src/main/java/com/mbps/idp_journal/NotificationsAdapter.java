package com.mbps.idp_journal;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mbps.idp_journal.model.Notifications;

import java.util.ArrayList;

public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int LAYOUT_HEADER = 0;
    private static final int LAYOUT_CHILD = 1;

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Notifications> notificationsList;

    public NotificationsAdapter(Context context, ArrayList<Notifications> notificationsList){

        inflater = LayoutInflater.from(context);
        this.context = context;
        this.notificationsList = notificationsList;

    }

    @Override
    public int getItemCount() {
        return notificationsList.size();
    }

    @Override
    public int getItemViewType(int position)
    {
        if(notificationsList.get(position).isHeader()) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;
        if(viewType==LAYOUT_HEADER){
            View view = inflater.inflate(R.layout.rv_header, parent, false);
            holder = new MyViewHolderHeader(view);
        }else {
            View view = inflater.inflate(R.layout.rv_child, parent, false);
            holder = new MyViewHolderChild(view);
        }


        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(holder.getItemViewType()== LAYOUT_HEADER)
        {
            MyViewHolderHeader vaultItemHolder = (MyViewHolderHeader) holder;
        }
        else {

            MyViewHolderChild vaultItemHolder = (MyViewHolderChild) holder;
            vaultItemHolder.tvChild.setText(notificationsList.get(position).getNotification().NotificationsDetails);
            vaultItemHolder.tvHeader.setText(notificationsList.get(position).getNotification().NotificationsTitle);

        }

    }


    class MyViewHolderHeader extends RecyclerView.ViewHolder{

        TextView tvHeader, tvChild;

        public MyViewHolderHeader(View itemView) {
            super(itemView);

            tvHeader = (TextView) itemView.findViewById(R.id.tvHeader);
        }

    }

    class MyViewHolderChild extends RecyclerView.ViewHolder{

        TextView tvChild, tvHeader;

        public MyViewHolderChild(View itemView) {
            super(itemView);

            tvChild = (TextView) itemView.findViewById(R.id.tvChild);
            tvHeader = (TextView) itemView.findViewById(R.id.tvHeader);
        }

    }
}
