package com.mbps.idp_journal;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

//import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.RequestParams;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignupActivity extends BaseActivity {

    enum Validation{
        USERNAME,
        EMAIL,
        PASSWORD
    }

    EditText editTextName;
    EditText editTextEmail;
    EditText editTextpassword;
    EditText editTextConfirmpassword;
    Button buttonSignup;

    ImageView selectedProfilePicture, agirl1, agirl2, agirl3, agirl4, agirl5, agirl6, aguy1, aguy2, aguy3, aguy4, aguy5, aguy6;
    String selectedAvatar = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        editTextName = findViewById(R.id.name);
        editTextEmail = findViewById(R.id.email);
        editTextpassword = findViewById(R.id.password);
        editTextConfirmpassword = findViewById(R.id.confirmpassword);
        buttonSignup = findViewById(R.id.signup);

        selectedProfilePicture = findViewById(R.id.selectedProfilePic);
        agirl1 = findViewById(R.id.agirl1);
        agirl2 = findViewById(R.id.agirl2);
        agirl3 = findViewById(R.id.agirl3);
        agirl4 = findViewById(R.id.agirl4);
        agirl5 = findViewById(R.id.agirl5);
        agirl6 = findViewById(R.id.agirl6);

        aguy1 = findViewById(R.id.aguy1);
        aguy2 = findViewById(R.id.aguy2);
        aguy3 = findViewById(R.id.aguy3);
        aguy4 = findViewById(R.id.aguy4);
        aguy5 = findViewById(R.id.aguy5);
        aguy6 = findViewById(R.id.aguy6);

        handleProfileSelection();

        buttonSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editTextName.getText().length() <=0){
                    showDialog(Validation.USERNAME);
                    return;
                }

                if(editTextEmail.getText().length() <=0 || !isValidEmail(editTextEmail.getText().toString())){
                    showDialog(Validation.EMAIL);
                    return;
                }

                System.out.println("PASSWORD:::: " + editTextpassword.getText().toString());

                if(!isValidPassword(editTextpassword.getText().toString())){
                    showDialog(Validation.PASSWORD);
                    return;
                }

//                if(!isValidPassword(editTextConfirmpassword.getText().toString())){
//                    showDialog(Validation.PASSWORD);
//                    return;
//                }

                if(editTextpassword.getText().toString().compareTo(editTextConfirmpassword.getText().toString().toString()) != 0){
                    showDialog(Validation.PASSWORD);
                    return;
                }

                signUp();
        }
        });


        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        dismissKeyboard();

        ImageView closeButton = (ImageView) findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void handleProfileSelection(){
        agirl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl1);
                selectedAvatar = "agirl1";
            }
        });

        agirl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl2);
                selectedAvatar = "agirl2";
            }
        });

        agirl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl3);
                selectedAvatar = "agirl3";
            }
        });

        agirl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl4);
                selectedAvatar = "agirl4";
            }
        });

        agirl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl5);
                selectedAvatar = "agirl5";
            }
        });

        agirl6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.agirl6);
                selectedAvatar = "agirl6";
            }
        });

        aguy1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy1);
                selectedAvatar = "aguy1";
            }
        });

        aguy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy2);
                selectedAvatar = "aguy2";
            }
        });

        aguy3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy3);
                selectedAvatar = "aguy3";
            }
        });

        aguy4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy4);
                selectedAvatar = "aguy4";
            }
        });

        aguy5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy5);
                selectedAvatar = "aguy5";
            }
        });

        aguy6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedProfilePicture.setImageResource(R.drawable.aguy6);
                selectedAvatar = "aguy6";
            }
        });
    }

    private void signUp(){

        String usernamename = editTextName.getText().toString();
        String email = editTextEmail.getText().toString();
        String password =editTextpassword.getText().toString();
        String profilePicturePath = getProfilePictureURL();

        String parameter = "{" +
                "  \"EMail_Address\": \"emailaddress\"," +
                "  \"Password\": \"password\"," +
                "  \"IsActive\": \"isactive\"," +
                "  \"IsAdmin\": \"isadmin\"," +
                "  \"Username\": \"username\"," +
                "  \"ProfilePicturePath\": \"profilepicturepath\"" +
                "}";

        RequestParams params = new RequestParams();
        params.put("EMail_Address", email); //testuser
        params.put("Password", password); //pass123
        params.put("IsActive", false);
        params.put("IsAdmin", false);
        params.put("Username", usernamename);
        params.put("ProfilePicturePath", profilePicturePath);

        showLoading();

        showUnsuccessfulDialog("MESSAGE ITO");
        hideLoading();
        return;

//        APIRestClient.client(SignupActivity.this).post("/api/IDP/signUpUser", params, new AsyncHttpResponseHandler() {
//
//            @Override
//            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
//
//                String response = new String(responseBody);
//
//                System.out.println("SIGNUP:::: " + response);
//
//                JSONArray jsonArray = null;
//                Users user;
//
//                try {
//                    jsonArray = new JSONArray(new String(response));
//                    String status = jsonArray.getJSONObject(0).get("Status").toString();
//                    String responseMessage = jsonArray.getJSONObject(0).get("Response").toString();
//                    System.out.println("STATUS::: " + status);
//
//                    if(status.compareTo("1") == 0){
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(SignupActivity.this);
//                        dialog.setCancelable(false);
//                        dialog.setTitle(R.string.successfulregistrationheader);
//                        dialog.setMessage(responseMessage);
//                        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int id) {
//                                finish();
//                            }
//                        });
//                        final AlertDialog alert = dialog.create();
//                        alert.show();
//                    }
//                    else {
//                        showUnsuccessfulDialog(new String(response));
//                    }
//
//                } catch (JSONException e) {
//                    showUnsuccessfulDialog(e.getMessage());
//                    e.printStackTrace();
//                }
//
//                hideLoading();
//            }
//
//
//            @Override
//            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
//                System.out.println("SIGNUP:::: " + responseBody);
//                Log.e("ERROR SIGNUP", "ERROR: " + new String(responseBody));
//                showUnsuccessfulDialog(new String(responseBody));
//            }
//
//
//            @Override
//            public void onFinish() {
//
//                super.onFinish();
//
//
//                hideLoading();
//            }
//        });

    }


    public void showUnsuccessfulDialog(String message){
        AlertDialog.Builder dialog = new AlertDialog.Builder(SignupActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(R.string.unsuccessfulregistrationheader);
        dialog.setMessage(getResources().getString(R.string.unsuccessfulregistrationmessage) + "\n\n" + message);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                hideLoading();
            }
        });
        final AlertDialog alert = dialog.create();
        alert.show();
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    static boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    private void showDialog(Validation validation){

        int header = 0;
        int message = 0;

        switch (validation){
            case USERNAME:

                header = R.string.invalidusernamemessage;
                message = R.string.invalidusernamemessage;

                break;
            case EMAIL:

                header = R.string.invalidemailheader;
                message = R.string.invalidemailmessage;

                break;
            case PASSWORD:

                header = R.string.invalidpasswordheader;
                message = R.string.invalidpasswordmessage;

                break;

            default:
        }

        AlertDialog.Builder dialog = new AlertDialog.Builder(SignupActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle(header);
        dialog.setMessage(message);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

    private String getProfilePictureURL(){
        return String.valueOf(selectedAvatar);
    }

    public void dismissKeyboard(){
        findViewById(R.id.parentlayout).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                System.out.println("MOTION:::" + event.getAction());

                if(event.getAction() == MotionEvent.ACTION_UP && event.getAction() != MotionEvent.ACTION_MOVE){
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    return true;
                }

                return false;
            }
        });
    }
}
