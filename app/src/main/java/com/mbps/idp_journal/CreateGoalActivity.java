package com.mbps.idp_journal;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mbps.idp_journal.api.APIRestClient;
import com.mbps.idp_journal.model.CompetencyBadges;
import com.mbps.idp_journal.model.Users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateGoalActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {

    Spinner spinnerCompetency, spinnerDevelopment;
    EditText editFrom,
            editDescription,
            editActionPlan,
            editResources,
            editTask1,
            editTask2,
            editTask3;
    ArrayList<CompetencyBadges> competencyBadgesList = new ArrayList<>();
    Button btnSubmit, btnAddSuccessMeasure;
    Calendar calendar;
    ImageView closeButton;

    int year, month, day;

    List<GoalItem> listGoals;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_goal);

        kConstants = new Constants(CreateGoalActivity.this);

        editFrom = findViewById(R.id.editfrom);
        editDescription = findViewById(R.id.editdescription);
        editActionPlan = findViewById(R.id.editactionplan);
        editResources = findViewById(R.id.editresources);

        editTask1 = findViewById(R.id.textTaskDesc1);
        editTask2 = findViewById(R.id.textTaskDesc2);
        editTask3 = findViewById(R.id.textTaskDesc3);

        btnSubmit = findViewById(R.id.btncreategoal);
        btnAddSuccessMeasure = findViewById(R.id.btnaddsuccess);

        btnAddSuccessMeasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTask2.getVisibility() == View.GONE) {
                    editTask2.setVisibility(View.VISIBLE);
                    return;
                }

                if(editTask3.getVisibility() == View.GONE) {
                    editTask3.setVisibility(View.VISIBLE);
                    btnAddSuccessMeasure.setVisibility(View.GONE);
                    return;
                }
            }
        });

        closeButton = findViewById(R.id.closeButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        spinnerCompetency = (Spinner) findViewById(R.id.competencyspinner);
        spinnerCompetency.setOnItemSelectedListener(this);

        getAllCompetencyBadges();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showLoading();

                try {

                    Gson json = new Gson();
                    String prefValue = kConstants.getPreferenceString(Constants.PREF_GOALLIST,"");
                    Type listGoalsType = new TypeToken<List<GoalItem>>() {}.getType();
                    listGoals = json.fromJson(prefValue, listGoalsType);
                    if(listGoals == null) {

                        listGoals = new ArrayList<>();
                    }
                }
                catch (Exception e) {
                    Log.e("ERROR", "ERROR" + e.getMessage());
                    listGoals = new ArrayList<>();
                }

                GoalItem item = new GoalItem();
                item.competencyTitle = competencyBadgesList.get(spinnerCompetency.getSelectedItemPosition()).Competency;
                item.goalDueDate = editFrom.getText().toString();
                item.goalDescription = editDescription.getText().toString();
                item.goalActionPlan = editActionPlan.getText().toString();
                item.goalResources = editResources.getText().toString();
                item.goalSuccessMeasure1 = editTask1.getText().toString();
                item.goalSuccessMeasure2 = editTask2.getText().toString();
                item.goalSuccessMeasure3 = editTask3.getText().toString();
                item.goalProgress = 0.0;

                listGoals.add(item);

                System.out.println("GOALDUEDATE:::::: " + editFrom.getText().toString());

                sendGoalPlan(item);

            }
        });


        setDatePicker();
        dismissKeyboard();

    }

    private void getAllCompetencyBadges(){

        competencyBadgesList = kConstants.getAllCompetencyBadges();

        if(competencyBadgesList != null && competencyBadgesList.size()>0){
            setSpinnerCompetencyList();
            return;
        }

        showLoading();
        APIRestClient.client(CreateGoalActivity.this).get("/api/IDP/getAllCompetencyBadges",null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                competencyBadgesList = new ArrayList<>();
                String response = new String(responseBody);

                System.out.println("BADGES::: " + response);

                JSONArray jsonArray = null;
                Users user;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject badges = jsonArray.getJSONObject(i);

                        CompetencyBadges competencyBadges = CompetencyBadges.Builder.newInstance()
                                .setCompetencyId(badges.getString("CompetencyId"))
                                .setCompetency(badges.getString("Competency"))
                                .setRemarks(badges.getString("Remarks"))
                                .setBadgeType(badges.getString("BadgeType"))
                                .setBadgePoints(badges.getString("BadgePoints"))
                                .build();

                        competencyBadgesList.add(competencyBadges);
                    }

                    kConstants.setCompetencyBadges("competencyBadges", competencyBadgesList);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
            }


            @Override
            public void onFinish() {

                super.onFinish();
                hideLoading();
                setSpinnerCompetencyList();

            }
        });

    }

    public void setSpinnerCompetencyList(){

        ArrayList<String> spinnerAdapterData = new ArrayList<>();
//        SpinnerAdapter adapter = new CompetencyAdapter(this, R.layout.competency_spinner, competencyBadgesList, getResources());

        ArrayList<String> competencyList = new ArrayList<>();

        for(int i=0; i<competencyBadgesList.size(); i++){
            competencyList.add(competencyBadgesList.get(i).Competency);
        }

        SpinnerAdapter adapter = new com.mbps.idp_journal.SpinnerAdapter(CreateGoalActivity.this, R.layout.adapter_spinner, competencyList, getResources());


        spinnerCompetency.setAdapter(adapter);

    }

    public void sendGoalPlan(GoalItem goalItem){

        Users user = kConstants.getLoggedInUser();

        RequestParams params = new RequestParams();
        params.put("CompetencyId", competencyBadgesList.get(spinnerCompetency.getSelectedItemPosition()).CompetencyId);//goalItem.competencyId);
        params.put("Description",goalItem.goalDescription);
        params.put("ActionPlan",goalItem.goalActionPlan);
        params.put("Support", goalItem.goalResources);
        params.put("TargetCompletionDate",goalItem.goalDueDate);
        params.put("CreatedBy",user.UserId);
        params.put("ModifiedBy",user.UserId);

        String parameters = "{";
        parameters += "\"CompetencyId\": " + competencyBadgesList.get(spinnerCompetency.getSelectedItemPosition()).CompetencyId + ", ";
        parameters += "\"Description\": \"" + goalItem.goalDescription + "\", ";
        parameters += "\"ActionPlan\": \"" + goalItem.goalActionPlan + "\", ";
        parameters += "\"Support\": \"" + goalItem.goalActionPlan + "\", ";
        parameters += "\"TargetCompletionDate\": \"" + goalItem.goalDueDate + "\", ";
        parameters += "\"CreatedBy\": " + user.UserId + ", ";
        parameters += "\"ModifiedBy\": " + user.UserId + ", ";

        List<Map<String, Object>> listParams = new ArrayList<>();
        String paramMeasurement = "[";
        for(int i = 1; i <= 3; i++) {

            String measure = "";
            if(i == 1 && editTask1.getText().toString().length() > 0) {
                measure = editTask1.getText().toString();
            }
            else if(i == 2 && editTask2.getText().toString().length() > 0) {
                paramMeasurement = paramMeasurement + ", ";
                measure = editTask2.getText().toString();
            }
            else if(i == 3 && editTask3.getText().toString().length() > 0) {
                paramMeasurement = paramMeasurement + ", ";
                measure = editTask3.getText().toString();
            }


            if(measure.length() > 0) {

                Map<String, Object> measureP = new HashMap<>();
                measureP.put("IDPId", (String) null);
                measureP.put("Measure", measure);
                measureP.put("IsCompleted", false);
                measureP.put("CreatedBy", Integer.valueOf(user.UserId));
                measureP.put("ModifiedBy", Integer.valueOf(user.UserId));

                listParams.add(measureP);

                String measurement = "{" +
                        "\"IDPId\": null," +
                        "\"Measure\": \"" + measure + "\"," +
                        "\"IsCompleted\": false," +
                        "\"CreatedBy\": " + Integer.valueOf(user.UserId) + "," +
                        "\"ModifiedBy\": " + Integer.valueOf(user.UserId) + "," +
                        "}";

                paramMeasurement = paramMeasurement + measurement;
            }
        }

        paramMeasurement = paramMeasurement + "]";


        params.put("Measurement", listParams);
        Log.e("ERROR PARAM", "PARAMETER: " + new Gson().toJson(params));

        showLoading();

        APIRestClient.client(CreateGoalActivity.this).post("/api/IDP/insertGoalPlan", params, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);


//                finish();

            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("GOALPLAN:::: " + responseBody);
                Log.e("ERROR FAILED", "response: " + new String(responseBody));
            }


            @Override
            public void onFinish() {

                super.onFinish();



                hideLoading();
                try {
                    Gson json = new Gson();
                    String prefValue = json.toJson(listGoals);

                    if(kConstants.setPreferenceString(Constants.PREF_GOALLIST, prefValue) == true){
                        finish();
                    }
                    else {
                        Log.e("ERROR", "ERROR SAVING PREF GOALS");
                    }
                }
                catch (Exception e) {
                    Log.e("ERROR", e.getMessage());
                }


            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

    }

    public void onNothingSelected(AdapterView<?> parent) {

    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {

        DatePickerDialog mDate = new DatePickerDialog(CreateGoalActivity.this, myDateListener, 2016, 2, 24);
        mDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        mDate.show();
        editFrom.setInputType(InputType.TYPE_NULL);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {

                    arg0.setCalendarViewShown(false);
                    arg0.setSpinnersShown(true);

                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day

                    calendar = Calendar.getInstance();
                    calendar.set(arg1, arg2, arg3);
                    showDate((new SimpleDateFormat("MMMM dd, yyyy")).format(calendar.getTime()));
                }
            };

    private void showDate(String date) {

        editFrom.setText(date);
        editFrom.setInputType(InputType.TYPE_NULL);
    }

    private void setDatePicker(){
        calendar = Calendar.getInstance();
        showDate((new SimpleDateFormat("MMMM dd, yyyy")).format(calendar.getTime()));


        editFrom.setInputType(InputType.TYPE_NULL);
    }


    public void dismissKeyboard(){
        findViewById(R.id.scrollLayout).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                System.out.println("MOTION:::" + event.getAction());

                if(event.getAction() == MotionEvent.ACTION_UP && event.getAction() != MotionEvent.ACTION_MOVE){
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    return true;
                }

                return false;
            }
        });
    }
}
