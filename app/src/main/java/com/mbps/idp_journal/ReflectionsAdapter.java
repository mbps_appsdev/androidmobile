package com.mbps.idp_journal;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class ReflectionsAdapter extends ArrayAdapter<String> {

    private ArrayList<String> mData;
    public Resources mResources;
    private LayoutInflater mInflater;

    public GoalProgressOverviewActivity delegate;


    public ReflectionsAdapter(
            Activity activitySpinner,
            int textViewResourceId,
            ArrayList objects,
            Resources resLocal
    ) {
        super(activitySpinner, textViewResourceId, objects);

        mData = objects;
        mResources = resLocal;
        mInflater = (LayoutInflater) activitySpinner.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = mInflater.inflate(R.layout.adapter_spinner, parent, false);
        TextView item = (TextView) row.findViewById(R.id.list_item);

        final String monthYear = mData.get(position);

        item.setText(monthYear);

        if(delegate != null) {
            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    delegate.progressSetSelected(Integer.valueOf(monthYear.split(" - ")[1]), Integer.valueOf(monthYear.split(" - ")[0]));
                }
            });
        }

        return row;
    }

}