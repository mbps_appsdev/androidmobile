package com.mbps.idp_journal;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.RelativeLayout;

public class SplashActivity extends AppCompatActivity {

    RelativeLayout splashLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        castViews();
    }


    public void castViews(){
        splashLayout = (RelativeLayout) findViewById(R.id.splash);
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setStartOffset(1000);
        fadeOut.setDuration(1000);

        AnimationSet animation = new AnimationSet(false);
        animation.addAnimation(fadeOut);
        splashLayout.setAnimation(animation);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent start = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(start);
//                finish();

            }
        }, 1800);
    }

}
