package com.mbps.idp_journal;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mbps.idp_journal.model.CompetencyBadges;
import com.mbps.idp_journal.model.Users;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by MobileDev on 17/11/2017.
 */

public class Constants {
    public static String PREFKEY = "com.mbps.idp_journal.preferences";

    public static String PREF_GOALLIST = "PREF_GOAL_LIST";

    public static String API_BASE = "http://13.229.176.204/IDP_API/";

    public static String FONT_BOLD = "Avenir_Bold.ttf";
    public static String FONT_REGULAR = "Avenir_Regular.ttf";

    // Constants used to pass extra data in the intent
    public static final String BarcodeObject = "Barcode";

    public static final int MESSAGE_NOTIFICATION_ID = 1;


    public static SharedPreferences mPreferences;
    public static SharedPreferences.Editor editor;

    private Context ctx;

    public Constants(){

    }

    public Constants(Context context) {
        this.ctx = context;
    }

    public String getPreferenceString(String key, String d){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        return mPreferences.getString(key, d);
    }

    public Float getPreferenceFloat(String key, Float d){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        return mPreferences.getFloat(key, d);
    }

    public Boolean getPreferenceBool(String key, Boolean d){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        return mPreferences.getBoolean(key, d);
    }

    public Boolean setPreferenceString(String key, String value){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        if(editor == null) {
            editor = mPreferences.edit();
        }

        editor.putString(key, value);


        return editor.commit();
    }

    public Boolean setPreferenceFloat(String key, Float value){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        if(editor == null) {
            editor = mPreferences.edit();
        }

        editor.putFloat(key, value);


        return editor.commit();
    }

    public Boolean setPreferenceBoolean(String key, Boolean value){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY,Context.MODE_PRIVATE);
        }

        if(editor == null) {
            editor = mPreferences.edit();
        }

        editor.putBoolean(key, value);


        return editor.commit();
    }

    public Boolean setCompetencyBadges(String key, ArrayList<CompetencyBadges> badges){
        if(mPreferences == null) {
            mPreferences = ((AppCompatActivity) ctx).getSharedPreferences(PREFKEY, Context.MODE_PRIVATE);
        }

        if(editor == null) {
            editor = mPreferences.edit();
        }

        Gson gson = new Gson();
        String serializedBadges = gson.toJson(badges);
        editor.putString(key, serializedBadges);

        return editor.commit();
    }

    public Boolean setLoggedInUser(Users user) {
        String userString = new Gson().toJson(user);
        return this.setPreferenceString("isLogin", userString);
    }

    public Users getLoggedInUser() {
        String userString = this.getPreferenceString("isLogin", null);

        Type type = new TypeToken<Users>() {}.getType();
        return new Gson().fromJson(userString, type);
    }

    public ArrayList<CompetencyBadges> getAllCompetencyBadges(){
        String deserializedBadges = this.getPreferenceString("competencyBadges", null);

        Type type = new TypeToken<ArrayList<CompetencyBadges>>() {}.getType();

        return new Gson().fromJson(deserializedBadges, type);
    }
}
