package com.mbps.idp_journal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mbps.idp_journal.api.APIRestClient;
import com.mbps.idp_journal.model.PlanDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class IDPJMainActivity extends BaseActivity{

    RecyclerView goalListView;
    GoalsAdapter adapter;
    Button btncreategoal;
    ImageView menuButton, createGoal, notifButton;
    RelativeLayout layoutNoGoals, layoutGoalList, layoutProgress;
    ArrayList<PlanDetails> planDetailList = new ArrayList<>();
    List<GoalItem> listGoals =  new ArrayList<>();
    Boolean isFirstLogin = true;

    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idpjmain);

        btncreategoal = findViewById(R.id.btncreategoal);
        createGoal = findViewById(R.id.addgoalbutton);
        menuButton = findViewById(R.id.menuButton);
        notifButton = findViewById(R.id.notifButton);
        layoutNoGoals = findViewById(R.id.layoutNoGoals);
        layoutGoalList = findViewById(R.id.layoutGoalList);
        layoutProgress = findViewById(R.id.progressIndicator);
        layoutProgress.setVisibility(View.VISIBLE);
        layoutGoalList.setVisibility(View.GONE);
        layoutNoGoals.setVisibility(View.GONE);

        createGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMain = new Intent(IDPJMainActivity.this, CreateGoalActivity.class);
                startActivity(intentMain);
            }
        });

        btncreategoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMain = new Intent(IDPJMainActivity.this, CreateGoalActivity.class);
                startActivity(intentMain);
            }
        });

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
            }
        });

        notifButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMain = new Intent(IDPJMainActivity.this, NotificationsActivity.class);
                startActivity(intentMain);
            }
        });

        dismissKeyboard();

    }

    public void getGoals(){

        if(kConstants.getLoggedInUser() != null){
            getPlanDetails(kConstants.getLoggedInUser().UserId);
        }
        else {
            finish();
        }

    }

    public void getPlanDetails(String userId){
        layoutProgress.setVisibility(View.VISIBLE);
        planDetailList = new ArrayList<>();

        APIRestClient.client(IDPJMainActivity.this).get("/api/IDP/getPlanDetails/UserId/"+userId,null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);
                System.out.println("BADGES::: " + response);
                JSONArray jsonArray = null;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject plan = jsonArray.getJSONObject(i);

                        PlanDetails planDetails = PlanDetails.Builder.newInstance()
                                .setIDPId(plan.getString("IDPId"))
                                .setCompetencyId(plan.getString("CompetencyId"))
                                .setCompetency(plan.getString("Competency"))
                                .setDescription(plan.getString("Remarks"))
                                .setActionPlan(plan.getString("ActionPlan"))
                                .setSupport(plan.getString("Support"))
                                .setTargetCompletionDate(plan.getString("TargetCompletionDate"))
                                .setActualCompletionDate(plan.getString("ActualCompletionDate"))
                                .setCreatedBy(plan.getString("CreatedBy"))
                                .setCreatedDate(plan.getString("CreatedDate"))
                                .setModifiedBy(plan.getString("ModifiedBy"))
                                .setModifiedDate(plan.getString("ModifiedBy"))
                                .setOverallProgress(plan.getInt("BadgeProgress"))
                                .setRemarks(plan.getString("Remarks"))
                                .setIsCompleted(plan.getBoolean("IsCompleted"))
                                .setSuccessMeasure(plan.getJSONArray("SuccessMeasure"))
                                .build();




                        planDetailList.add(planDetails);

                        for(int j=0; j<planDetails.SuccessMeasure.size(); j++){
                            System.out.println("SUCCESSMEASURE:::: " + planDetails.SuccessMeasure.get(j).IsCompleted);
                        }



                    }

                    if(planDetailList.size() == 0) {

                        layoutProgress.setVisibility(View.GONE);

                        layoutGoalList.setVisibility(View.GONE);
                        layoutNoGoals.setVisibility(View.VISIBLE);
                    }
                    else {

                        goalListView = (RecyclerView) findViewById(R.id.recyclerGoals);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(IDPJMainActivity.this);
                        goalListView.setLayoutManager(mLayoutManager);
                        goalListView.setItemAnimator(new DefaultItemAnimator());
                        goalListView.setNestedScrollingEnabled(false);
                        goalListView.setHasFixedSize(false);

                        adapter = new GoalsAdapter(IDPJMainActivity.this, planDetailList);
                        adapter.mClickListener = (new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Gson gson = new Gson();
                                String serializedGoals = gson.toJson(planDetailList.get(Integer.parseInt(v.getTag().toString())));

                                Intent intentMain = new Intent(IDPJMainActivity.this, GoalProgressOverviewActivity.class);
                                PlanDetails goalItem = planDetailList.get(Integer.parseInt(v.getTag().toString()));
                                intentMain.putExtra("goal", new Gson().toJson(goalItem));
                                startActivity(intentMain);
                            }
                        });

                        goalListView.setAdapter(adapter);

                        layoutProgress.setVisibility(View.GONE);

                        layoutGoalList.setVisibility(View.VISIBLE);
                        layoutNoGoals.setVisibility(View.GONE);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("FAILED");
            }


            @Override
            public void onFinish() {

                super.onFinish();


            }
        });
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        return;
    }

    @Override
    protected void onResume() {
        super.onResume();

        getGoals();
    }
}
