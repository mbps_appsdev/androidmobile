package com.mbps.idp_journal;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mbps.idp_journal.model.PlanDetails;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class GoalsAdapter extends RecyclerView.Adapter<GoalsAdapter.ViewHolder> {

    View.OnClickListener mClickListener;
    Context mContext;
    LayoutInflater inflater;

    ArrayList<PlanDetails> items;

    public GoalsAdapter(Context mContext, ArrayList<PlanDetails> _items) {
        this.mContext = mContext;
        this.inflater = LayoutInflater.from(mContext);
        this.items = _items;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layout;
        public ViewHolder(View view){
            super(view);
            layout = (LinearLayout) view;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public GoalsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {


        LinearLayout itemView = (LinearLayout) LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_goals, viewGroup, false);

        GoalsAdapter.ViewHolder holder = new GoalsAdapter.ViewHolder(itemView);

        return (GoalsAdapter.ViewHolder) holder;

    }

    @Override
    public void onBindViewHolder(GoalsAdapter.ViewHolder viewHolder, int i) {


        TextView tvRank = (TextView) viewHolder.layout.findViewById(R.id.textGoalCompetency);
        TextView tvDescription = (TextView) viewHolder.layout.findViewById(R.id.textGoalDescription);
        ProgressBar progressBar = (ProgressBar) viewHolder.layout.findViewById(R.id.progressGoal);
        TextView tvProgress = (TextView) viewHolder.layout.findViewById(R.id.progressGoalsText);
        TextView tvDueDate = (TextView) viewHolder.layout.findViewById(R.id.textDueDate);
        SeekBar seekBar = (SeekBar) viewHolder.layout.findViewById(R.id.progressGoal);

        PlanDetails planDetails = items.get(i);

        tvRank.setText(planDetails.Competency);
        tvDescription.setText(planDetails.Description);
        seekBar.setEnabled(false);
        int progress = planDetails.OverallProgress;

        System.out.println("PROGRES::::: " + progress);

        progressBar.setProgress(progress);
        tvProgress.setText(String.valueOf(progress) + "%");

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        SimpleDateFormat sdf2 = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);

        Date date = null;
        try {
            date = sdf1.parse(planDetails.TargetCompletionDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dueDate = sdf2.format(date);

        tvDueDate.setText("Due on " + dueDate);

        viewHolder.layout.setOnClickListener(mClickListener);
        viewHolder.layout.setTag(i);

    }


    @Override
    public int getItemCount() {
        return items.size();
    }
}
