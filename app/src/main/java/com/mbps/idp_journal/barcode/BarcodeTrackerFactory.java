package com.mbps.idp_journal.barcode;

import android.content.Context;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by jhec on 06/07/2017.
 */

public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode>{

    private Context mContext;

    public BarcodeTrackerFactory(Context context) {
        mContext = context;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        return new BarcodeTracker(mContext);
    }

}
