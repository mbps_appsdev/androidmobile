package com.mbps.idp_journal;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.mbps.idp_journal.api.APIRestClient;
import com.mbps.idp_journal.model.PlanDetails;
import com.mbps.idp_journal.model.ReflectionItem;
import com.mbps.idp_journal.model.Reflections;
import com.mbps.idp_journal.model.SuccessMeasure;
import com.mbps.idp_journal.model.Users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class GoalProgressOverviewActivity extends BaseActivity {

    static final int CERTIFICATES = 1;
    Button buttonDone, buttonSave, btncreatereflection;
    ImageView backarrow, edit;
    View overviewSelector, progressSelector, divider;
    TextView txtOverview, txtProgress, txtgoalCompetencyTitle;
    TextView textExcitedText, textHappyText, textNeutralText, textSadText, textWorriedText;
    Spinner spinner;
    ScrollView paneOverview, paneProgress;
    EditText description, actionplan, resources, duedate;
    EditText editTextlearned, editTextNextTask;
    EditText editTextHowAreYouFeeling, editTextWhatWillIdoNext;

    CheckBox checkBox1, checkBox2, checkBox3;

    RelativeLayout addentry, cancelentry, editgoal, editcancel,noreflections, reflections;

    RelativeLayout layoutExcited, layoutHappy, layoutNeutral, layoutSad, layoutWorried;
    SeekBar seekBar;

    TextView progressGoalsText;
    Calendar calendar;
    int year, month, day;

    int currentProgressValue = 0;

    PlanDetails planDetailItem;
    TimePickerDialog.OnTimeSetListener mOnTimeSetListener;

    String selectedFeeling = "neutral";

    ArrayList<Reflections> spinnerAdapterData = new ArrayList<>();

    ArrayList<String> sortByDateArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_progressoverview);

        castViews();

        progressSelector.setVisibility(View.GONE);

        setClickListener();

        planDetailItem = deserializedGoals();

        txtgoalCompetencyTitle.setText(planDetailItem.Competency);
        description.setText(planDetailItem.Description);
        actionplan.setText(planDetailItem.ActionPlan);
        resources.setText(planDetailItem.Support);

        for(int i=0; i<planDetailItem.SuccessMeasure.size(); i++){

            if(i == 0){
                checkBox1.setChecked(planDetailItem.SuccessMeasure.get(i).IsCompleted);
            }
            else if(i == 1){
                checkBox2.setChecked(planDetailItem.SuccessMeasure.get(i).IsCompleted);
            }
            else if(i == 2){
                checkBox3.setChecked(planDetailItem.SuccessMeasure.get(i).IsCompleted);
            }
        }

        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(planDetailItem.TargetCompletionDate));
        DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
        String dueDate = df.format(cal.getTime());

        duedate.setText(dueDate);



        Log.e("ERRORR", "success measures: " + planDetailItem.SuccessMeasure);
        for(int i=0; i<planDetailItem.SuccessMeasure.size(); i++){
            if(planDetailItem.SuccessMeasure.size() > 0) {


                SuccessMeasure successMeasure = planDetailItem.SuccessMeasure.get(i);


                try {
                    CheckBox box = checkBox1;
                    if(i == 0) {
                        box = checkBox1;
                    }
                    else if(i == 1) {
                        box = checkBox2;
                    }
                    else if(i == 2) {
                        box = checkBox3;
                    }

                    box.setText(successMeasure.Measure);
                    box.setVisibility(View.VISIBLE);
                    box.setSelected(successMeasure.IsCompleted);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progressChangedValue = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressGoalsText.setText(String.valueOf(progress) + "%");
                progressChangedValue = progress;
                seekBar.setProgress(progressChangedValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                progressGoalsText.setText(String.valueOf(seekBar.getProgress()) + "%");
                seekBar.setProgress(progressChangedValue);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            public void onItemSelected(AdapterView<?> arg0,
                                       View arg1, int arg2, long arg3)
            {
                int index = arg0.getSelectedItemPosition();
                progressSetSelected(Integer.valueOf(spinnerAdapterData.get(index).Month), Integer.valueOf(spinnerAdapterData.get(index).Year));

            }
            public void onNothingSelected(AdapterView<?> arg0){

            }
        });

        description.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        actionplan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        resources.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        duedate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        editTextlearned.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        editTextNextTask.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        dismissKeyboard();

        System.out.println("GOAL DETAILS::::::: " + planDetailItem.ActionPlan);

    }

    private void castViews(){

        buttonDone = findViewById(R.id.buttonDone);
        backarrow = findViewById(R.id.backButton);
        overviewSelector = findViewById(R.id.viewMenuOverviewSelector);
        progressSelector = findViewById(R.id.viewMenuProgressSelector);
        paneOverview = findViewById(R.id.layoutOverview);
        paneProgress = findViewById(R.id.layoutProgress);
        txtOverview = findViewById(R.id.menuOverview);
        txtProgress = findViewById(R.id.menuProgress);
        description = findViewById(R.id.editdescription);
        actionplan = findViewById(R.id.editactionplan);
        resources = findViewById(R.id.editresources);
        addentry = findViewById(R.id.addEntryButton);
        cancelentry = findViewById(R.id.cancelEntryButton);
        buttonSave = findViewById(R.id.buttonSave);
        editgoal = findViewById(R.id.editButton);
        editcancel = findViewById(R.id.editCancel);
        divider = findViewById(R.id.divider);
        progressGoalsText = findViewById(R.id.progressGoalsText);
        spinner = findViewById(R.id.spinnerDateFilter);
        edit = findViewById(R.id.imgEdit);
        duedate = findViewById(R.id.editfrom);
        txtgoalCompetencyTitle = findViewById(R.id.goalCompetencyTitle);
        editTextlearned = (EditText) findViewById(R.id.textTitleLearnedDetails);
        editTextNextTask = (EditText) findViewById(R.id.textTitleNextDetails);
        checkBox1 = findViewById(R.id.checkbox1);
        checkBox2 = findViewById(R.id.checkbox2);
        checkBox3 = findViewById(R.id.checkbox3);
        seekBar = findViewById(R.id.seekbarGoal);
        seekBar.setEnabled(false);
        layoutExcited = findViewById(R.id.layoutExcited);
        layoutHappy = findViewById(R.id.layoutHappy);
        layoutNeutral = findViewById(R.id.layoutNeutral);
        layoutSad = findViewById(R.id.layoutSad);
        layoutWorried = findViewById(R.id.layoutWorried);
        textSadText = findViewById(R.id.textSadText);
        textExcitedText = findViewById(R.id.textExcitedText);
        textHappyText = findViewById(R.id.textHappyText);
        textWorriedText = findViewById(R.id.textWorriedText);
        textNeutralText = findViewById(R.id.textNeutralText);
        btncreatereflection = findViewById(R.id.btncreaterefelection);
        noreflections = findViewById(R.id.noreflections);
        reflections = findViewById(R.id.noreflections);
        textHappyText.setTypeface(Typeface.DEFAULT_BOLD);
        noreflections.setVisibility(View.GONE);
    }

    private PlanDetails deserializedGoals(){

        Gson gson = new Gson();
        Intent intent = getIntent();
        String serializedGoals = intent.getStringExtra("goal").toString();

        return gson.fromJson(serializedGoals, PlanDetails.class);
    }

    private void setSpinnerAdapter(){

        System.out.println("SPINNERDATA" + spinnerAdapterData);

        for(int i=0; i<spinnerAdapterData.size(); i++){
            String item = spinnerAdapterData.get(i).Year.toString() + " - " + spinnerAdapterData.get(i).Month.toString();
            if(sortByDateArray.contains(item) == false){

                sortByDateArray.add(item);
            }
        }

        ReflectionsAdapter adapter = new ReflectionsAdapter(this, R.layout.competency_spinner, sortByDateArray, getResources());
        adapter.delegate = GoalProgressOverviewActivity.this;


        spinner.setAdapter(adapter);

        if(sortByDateArray.size() == 0) {
            noreflections.setVisibility(View.VISIBLE);
            paneProgress.setVisibility(View.GONE);
        }
        else {

            noreflections.setVisibility(View.GONE);
            paneProgress.setVisibility(View.VISIBLE);
        }
    }

    private void setClickListener(){

        editgoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setBackgroundResource(R.drawable.goaledittextborder);
                actionplan.setBackgroundResource(R.drawable.goaledittextborder);
                resources.setBackgroundResource(R.drawable.goaledittextborder);
                description.setEnabled(true);
                actionplan.setEnabled(true);
                resources.setEnabled(true);
                editcancel.setVisibility(View.VISIBLE);
                editgoal.setVisibility(View.GONE);
                buttonDone.setVisibility(View.VISIBLE);
                checkBox1.setEnabled(true);
                checkBox2.setEnabled(true);
                checkBox3.setEnabled(true);
            }
        });

        editcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setBackgroundResource(0);
                actionplan.setBackgroundResource(0);
                resources.setBackgroundResource(0);
                description.setEnabled(false);
                actionplan.setEnabled(false);
                resources.setEnabled(false);
                buttonDone.setVisibility(View.GONE);
                editcancel.setVisibility(View.GONE);
                editgoal.setVisibility(View.VISIBLE);
                checkBox1.setEnabled(false);
                checkBox2.setEnabled(false);
                checkBox3.setEnabled(false);
            }
        });

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setBackgroundResource(0);
                actionplan.setBackgroundResource(0);
                resources.setBackgroundResource(0);
                description.setEnabled(false);
                actionplan.setEnabled(false);
                resources.setEnabled(false);
                buttonDone.setVisibility(View.GONE);
//                submitReflection(planDetailItem.IDPId);
                updateGoalPlan();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addentry.setVisibility(View.VISIBLE);
                cancelentry.setVisibility(View.GONE);
                description.setBackgroundResource(0);
                editTextlearned.setBackgroundResource(0);
                editTextNextTask.setBackgroundResource(0);
                editTextlearned.setEnabled(false);
                editTextNextTask.setEnabled(false);
                buttonSave.setVisibility(View.GONE);
                currentProgressValue = seekBar.getProgress();

                submitReflection(planDetailItem.IDPId);
            }
        });

        addentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addentry.setVisibility(View.GONE);
                cancelentry.setVisibility(View.VISIBLE);
                description.setBackgroundResource(R.drawable.goaledittextborder);
                editTextlearned.setBackgroundResource(R.drawable.goaledittextborder);
                editTextNextTask.setBackgroundResource(R.drawable.goaledittextborder);
                editTextlearned.setEnabled(true);
                editTextNextTask.setEnabled(true);
                editTextlearned.setText("");
                editTextNextTask.setText("");
                seekBar.setProgress(currentProgressValue);
                seekBar.setVisibility(View.VISIBLE);
                buttonSave.setVisibility(View.VISIBLE);
                seekBar.setEnabled(true);
                layoutNeutral.performClick();
                spinner.setVisibility(View.GONE);
                divider.setVisibility(View.VISIBLE);
            }});

        cancelentry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addentry.setVisibility(View.VISIBLE);
                cancelentry.setVisibility(View.GONE);
                description.setBackgroundResource(0);
                editTextlearned.setBackgroundResource(0);
                editTextNextTask.setBackgroundResource(0);
                editTextlearned.setEnabled(false);
                editTextNextTask.setEnabled(false);
                buttonSave.setVisibility(View.GONE);
                seekBar.setEnabled(false);
                seekBar.setProgress(currentProgressValue);
                spinner.setVisibility(View.VISIBLE);
                divider.setVisibility(View.GONE);

                if(sortByDateArray.size() == 0) {
                    overviewSelector.setVisibility(View.GONE);
                    progressSelector.setVisibility(View.VISIBLE);
                    paneOverview.setVisibility(View.GONE);
                    noreflections.setVisibility(View.VISIBLE);
                    paneProgress.setVisibility(View.GONE);
                }
            }
        });

        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
//                Intent intentMain = new Intent(GoalProgressOverviewActivity.this, IDPJMainActivity.class);
//                startActivity(intentMain);
            }
        });

        txtOverview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overviewSelector.setVisibility(View.VISIBLE);
                progressSelector.setVisibility(View.GONE);
                paneOverview.setVisibility(View.VISIBLE);
                paneProgress.setVisibility(View.GONE);
                noreflections.setVisibility(View.GONE);
            }
        });

        txtProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                overviewSelector.setVisibility(View.GONE);
                progressSelector.setVisibility(View.VISIBLE);
                paneOverview.setVisibility(View.GONE);
                noreflections.setVisibility(View.GONE);
                paneProgress.setVisibility(View.GONE);

                if(kConstants.getLoggedInUser() != null){
                    getReflectionId(kConstants.getLoggedInUser().UserId, planDetailItem.IDPId);
                }
                else {
                    finish();
                }
            }
        });


        layoutExcited.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEmoji(R.id.layoutExcited);
                textExcitedText.setTypeface(null, Typeface.BOLD);
                textHappyText.setTypeface(null, Typeface.NORMAL);
                textNeutralText.setTypeface(null, Typeface.NORMAL);
                textSadText.setTypeface(null, Typeface.NORMAL);
                textWorriedText.setTypeface(null, Typeface.NORMAL);
            }
        });

        layoutHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEmoji(R.id.layoutHappy);
                textHappyText.setTypeface(null, Typeface.BOLD);
                textExcitedText.setTypeface(null, Typeface.NORMAL);
                textNeutralText.setTypeface(null, Typeface.NORMAL);
                textSadText.setTypeface(null, Typeface.NORMAL);
                textWorriedText.setTypeface(null, Typeface.NORMAL);
            }
        });

        layoutNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEmoji(R.id.layoutNeutral);
                textHappyText.setTypeface(null, Typeface.NORMAL);
                textExcitedText.setTypeface(null, Typeface.NORMAL);
                textNeutralText.setTypeface(null, Typeface.BOLD);
                textSadText.setTypeface(null, Typeface.NORMAL);
                textWorriedText.setTypeface(null, Typeface.NORMAL);
            }
        });

        layoutSad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEmoji(R.id.layoutSad);
                textHappyText.setTypeface(null, Typeface.NORMAL);
                textExcitedText.setTypeface(null, Typeface.NORMAL);
                textNeutralText.setTypeface(null, Typeface.NORMAL);
                textSadText.setTypeface(null, Typeface.BOLD);
                textWorriedText.setTypeface(null, Typeface.NORMAL);
            }
        });

        layoutWorried.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectEmoji(R.id.layoutWorried);
                textWorriedText.setTypeface(null, Typeface.BOLD);
            }
        });

        btncreatereflection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paneProgress.setVisibility(View.VISIBLE);
                noreflections.setVisibility(View.GONE);

                addentry.setVisibility(View.GONE);
                cancelentry.setVisibility(View.VISIBLE);

                description.setBackgroundResource(R.drawable.goaledittextborder);
                editTextlearned.setBackgroundResource(R.drawable.goaledittextborder);
                editTextNextTask.setBackgroundResource(R.drawable.goaledittextborder);
                editTextlearned.setEnabled(true);
                editTextNextTask.setEnabled(true);
                editTextlearned.setText("");
                editTextNextTask.setText("");
                seekBar.setProgress(currentProgressValue);
                seekBar.setVisibility(View.VISIBLE);
                buttonSave.setVisibility(View.VISIBLE);
                seekBar.setEnabled(true);
                layoutNeutral.performClick();
                spinner.setVisibility(View.GONE);
            }
        });
    }

    public void selectEmoji(int id){

        switch (id) {
            case R.id.layoutExcited:
                selectedFeeling = "excited";
                layoutExcited.setAlpha(1.0f);
                layoutHappy.setAlpha(0.4f);
                layoutNeutral.setAlpha(0.4f);
                layoutSad.setAlpha(0.4f);
                layoutWorried.setAlpha(0.4f);
                break;
            case R.id.layoutHappy:
                selectedFeeling = "happy";
                layoutExcited.setAlpha(0.4f);
                layoutHappy.setAlpha(1.0f);
                layoutNeutral.setAlpha(0.4f);
                layoutSad.setAlpha(0.4f);
                layoutWorried.setAlpha(0.4f);
                break;
            case R.id.layoutNeutral:
                selectedFeeling = "neutral";
                layoutExcited.setAlpha(0.4f);
                layoutHappy.setAlpha(0.4f);
                layoutNeutral.setAlpha(1.0f);
                layoutSad.setAlpha(0.4f);
                layoutWorried.setAlpha(0.4f);
                break;
            case R.id.layoutSad:
                selectedFeeling = "sad";
                layoutExcited.setAlpha(0.4f);
                layoutHappy.setAlpha(0.4f);
                layoutNeutral.setAlpha(0.4f);
                layoutSad.setAlpha(1.0f);
                layoutWorried.setAlpha(0.4f);
                break;
            case R.id.layoutWorried:
                selectedFeeling = "worried";
                layoutExcited.setAlpha(0.4f);
                layoutHappy.setAlpha(0.4f);
                layoutNeutral.setAlpha(0.4f);
                layoutSad.setAlpha(0.4f);
                layoutWorried.setAlpha(1.0f);
                break;

        };
    }

    private void getReflectionId(String userId, String IDPId){

        showLoading();
        Log.e("ERR", "LOG: " + userId + ", " + IDPId);
        APIRestClient.client(GoalProgressOverviewActivity.this).get("/api/IDP/getReflection/UserId/"+userId+"/IdpId/"+IDPId,null, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                String response = new String(responseBody);

                System.out.println("BADGES::: " + response);

                JSONArray jsonArray = null;
                Users user;

                try {
                    jsonArray = new JSONArray(new String(response));

                    for(int i=0; i<jsonArray.length(); i++){

                        JSONObject array = jsonArray.getJSONObject(i);

                        Reflections reflections = Reflections.Builder.newInstance()
                                .setIDP_ReflectionId(array.getString("IDP_ReflectionId"))
                                .setIDPId(array.getString("IDPId"))
                                .setYear(array.getString("Year"))
                                .setMonth(array.getString("Month"))
                                .setConfigId(array.getString("ConfigId"))
                                .setConfigValue(array.getString("ConfigValue"))
                                .setReflectionType(array.getString("ReflectionType"))
                                .setReflection(array.getString("Reflection"))
                                .setCreatedBy(array.getString("CreatedBy"))
                                .setCreatedDate(array.getString("CreatedDate"))
                                .setModifiedBy(array.getString("ModifiedBy"))
                                .setModifiedDate(array.getString("ModifiedDate"))
                                .build();

                        spinnerAdapterData.add(reflections);
                    }

                    System.out.println("SPINNERADAPTERDATA::::: " + spinnerAdapterData);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("ERROR::::::");
            }

            @Override
            public void onFinish() {

                super.onFinish();
hideLoading();
                setSpinnerAdapter();
            }
        });
    }

    private void submitReflection(String idpId){

        showLoading();

        ArrayList<ReflectionItem> reflectionItems = new ArrayList<ReflectionItem>();

        ReflectionItem item = new ReflectionItem();
        item.Year = 0;
        item.Month = 0;
        item.ModifiedBy = 0;
        item.ReflectionType = "";

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());

        Users user = kConstants.getLoggedInUser();

        List<Map<String, Object>> listParams = new ArrayList<>();

        JSONObject paramObj = new JSONObject();
        JSONArray array = new JSONArray();

        for(int i = 1; i <= 4; i++) {

            String value = "";
            if(i == 1) {
                value = String.valueOf(seekBar.getProgress());
            }
            else if(i == 2) {
                value = selectedFeeling;
            }
            else if(i == 3) {
                value = editTextlearned.getText().toString();
            }
            else if(i == 4) {
                value = editTextNextTask.getText().toString();
            }

            Map<String, Object> reflection = new HashMap<>();
            reflection.put("Year", calendar.get(Calendar.YEAR));
            reflection.put("Month", calendar.get(Calendar.MONTH) + 1);
            reflection.put("ReflectionType", "IDP_Reflection" + i);
            reflection.put("Reflection", value);
            reflection.put("CreatedBy", Integer.valueOf(user.UserId));
            reflection.put("ModifiedBy", Integer.valueOf(user.UserId));

            listParams.add(reflection);


            try {

                JSONObject reflectionobj = new JSONObject();
                reflectionobj.put("Year", calendar.get(Calendar.YEAR));
                reflectionobj.put("Month", calendar.get(Calendar.MONTH) + 1);
                reflectionobj.put("ReflectionType", "IDP_Reflection" + i);
                reflectionobj.put("Reflection", value);
                reflectionobj.put("CreatedBy", Integer.valueOf(user.UserId));
                reflectionobj.put("ModifiedBy", Integer.valueOf(user.UserId));
                array.put(reflectionobj);
            }
            catch (Exception e) {

            }
        }

        try {
            paramObj.put("reflectionsDetails", array);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String json = "[{\"Reflection\":\"98\",\"CreatedBy\":54,\"Month\":8,\"ModifiedBy\":54,\"ReflectionType\":\"IDP_Reflection1\",\"Year\":2019},{\"Reflection\":\"excited\",\"CreatedBy\":54,\"Month\":8,\"ModifiedBy\":54,\"ReflectionType\":\"IDP_Reflection2\",\"Year\":2019},{\"Reflection\":\"learned new things\",\"CreatedBy\":54,\"Month\":8,\"ModifiedBy\":54,\"ReflectionType\":\"IDP_Reflection3\",\"Year\":2019},{\"Reflection\":\"nothing\",\"CreatedBy\":54,\"Month\":8,\"ModifiedBy\":54,\"ReflectionType\":\"IDP_Reflection4\",\"Year\":2019}]";

        RequestParams params = new RequestParams();
        params.put("reflectionsDetails", json);

        Log.e("ERROR", "LIST: " + new Gson().toJson(listParams));
        Log.e("ERROR", "[" + idpId + ":" + user.UserId + "]PARAMS: " + params);

        System.out.println("REFPARAMS::::::: " + params);

        showLoading();
        APIRestClient.client(GoalProgressOverviewActivity.this).postJSONARRAY("/api/IDP/insertUpdateReflection/idpId/" + idpId + "/userId/"+user.UserId, array, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                Log.e("ERROR", "response: " + new String(responseBody));

                try {
                    JSONObject jsonObject = new JSONObject(new String(responseBody));
                    Float progress = Float.valueOf((float) jsonObject.getDouble("ResponseItem"));

                    if(progress == 100.0){
                        BadgeDialog dialog=new BadgeDialog(GoalProgressOverviewActivity.this);
                        dialog.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                hideLoading();
            }


            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

                Log.e("ERROR", "error: " + error.getMessage());
                Log.e("ERROR", "[" + statusCode + "]FAILEDD response: " + new String(responseBody));
            }


            @Override
            public void onFinish() {

                super.onFinish();

                hideLoading();
            }
        });

    }

    public void updateGoalPlan(){

        try {

            JSONObject params = new JSONObject();
            params.put("IDP_Id", planDetailItem.IDPId);
            params.put("Description", description.getText());
            params.put("ActionPlan", actionplan.getText());
            params.put("Support", resources.getText());
            params.put("ModifiedBy", planDetailItem.ModifiedBy);

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(new Date());

            Users user = kConstants.getLoggedInUser();

            List<Map<String, Object>> listParams = new ArrayList<>();

            JSONObject paramObj = new JSONObject();
            JSONArray array = new JSONArray();


            for(int i=0; i<=planDetailItem.SuccessMeasure.size(); i++){
                try {

                    JSONObject reflectionobj = new JSONObject();
                    reflectionobj.put("IDP_MeasurementId", planDetailItem.SuccessMeasure.get(i).IDP_MeasurementId);//calendar.get(Calendar.YEAR));
                    reflectionobj.put("IDP_Id", planDetailItem.SuccessMeasure.get(i).IDP_Id);

                    switch (i){
                        case 0:
                            reflectionobj.put("IsCompleted", checkBox1.isChecked());
                            break;
                        case 1:
                            reflectionobj.put("IsCompleted", checkBox2.isChecked());
                            break;
                        case 2:
                            reflectionobj.put("IsCompleted", checkBox3.isChecked());
                            break;
                        case 3:
                            reflectionobj.put("IsCompleted", checkBox3.isChecked());
                            break;
                    }

                    reflectionobj.put("ModifiedBy", planDetailItem.ModifiedBy);

                    array.put(reflectionobj);
                }
                catch (Exception e) {

                }
            }

            params.put("Measurement", array);
            System.out.println("PARAMS:::::: " + params);

            showLoading();
            APIRestClient.client(GoalProgressOverviewActivity.this).putJSONOBJ("/api/IDP/updateGoalPlan/", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {

                    Log.e("ERROR", "response: " + new String(responseBody));

                    try {
                        JSONObject jsonObject = new JSONObject(new String(responseBody));
                        Float progress = Float.valueOf((float) jsonObject.getDouble("ResponseItem"));

                        if(progress == 100.0){
                            BadgeDialog dialog=new BadgeDialog(GoalProgressOverviewActivity.this);
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    hideLoading();
                }


                @Override
                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {

                    Log.e("ERROR", "error: " + error.getMessage());
                    Log.e("ERROR", "[" + statusCode + "]FAILEDD response: " + new String(responseBody));
                }


                @Override
                public void onFinish() {

                    super.onFinish();

                    hideLoading();
                }
            });
        }
        catch (Exception e) {

        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void progressSetSelected(Integer month, Integer year){

        for (int idx = 0; idx < spinnerAdapterData.size(); idx++) {
            Reflections i = spinnerAdapterData.get(idx);
            if(Integer.valueOf(i.Year).equals(year) && Integer.valueOf(i.Month).equals(month)) {

                Log.e("ERROR", "selected: " + month + ", " + year + " = " + i.ReflectionType);

                if(i.ReflectionType.equals("IDP_Reflection1")) {
                    //progress
                    seekBar.setProgress(Integer.valueOf(i.Reflection));
                }
                else if(i.ReflectionType.equals("IDP_Reflection2")) {
                    //progress
                    if(i.Reflection.equals("excited")) {
                        layoutExcited.performClick();
                    }
                    else if(i.Reflection.equals("happy")) {
                        layoutHappy.performClick();
                    }
                    else if(i.Reflection.equals("neutral")) {
                        layoutNeutral.performClick();
                    }
                    else if(i.Reflection.equals("sad")) {
                        layoutSad.performClick();
                    }
                    else if(i.Reflection.equals("worried")) {
                        layoutWorried.performClick();
                    }
                }
                else if(i.ReflectionType.equals("IDP_Reflection3")) {
                    //feeling
                    editTextlearned.setText(i.Reflection);
                }
                else if(i.ReflectionType.equals("IDP_Reflection4")) {
                    //next to do
                    editTextNextTask.setText(i.Reflection);
                }

            }
        }
    }

}
